package com.enigma.solutions;

import com.enigma.pojo.*;
import com.enigma.solutions.graph.*;
import com.enigma.solutions.moves.*;
import org.springframework.boot.autoconfigure.condition.*;
import org.springframework.stereotype.*;

import java.util.*;

@Component
@ConditionalOnProperty(name = "solver.impl", havingValue = "BFS")
public class BreadthFirstSearchSolver extends PuzzleSolver {

    @Override
    public Solution solvePuzzle(Puzzle puzzle) {
        ArrayList<Block> playerBlocks = puzzle.getMap().getPlayerBlocks();
        ArrayList<Block> winBlocks = puzzle.getMap().getEndBlocks();

        if (playerBlocks.size() != 1 || winBlocks.size() != 1) {
            return null;
        } else {
            return this.findSolutionForPlayerBlock(playerBlocks.get(0), puzzle);
        }
    }

    private Solution findSolutionForPlayerBlock(Block playerBlock, Puzzle puzzle) {
        long start = System.currentTimeMillis();
        PuzzleSolutionGraph solutionGraph  = new PuzzleSolutionGraph(playerBlock.getX(), playerBlock.getY());
        HashMap<SolutionNode, Solution> possibleSolutions = new HashMap<>();

        for (int i = 0; (i < this.maxSolutionLength && solutionGraph.hasNewLeafNodes()); i++) {
            ArrayList<SolutionNode> leafNodes = solutionGraph.getLeafNodes();
            for (SolutionNode leaf : leafNodes) {
                for (Direction direction : Direction.values()) {
                    if (leaf.getNodeStatus().equals(SolutionNodeState.REDIRECTION) && !leaf.getNewDirection().equals(direction)) {
                        continue;
                    }

                    SolutionNode child = this.moveSimulator.simulateMove(puzzle, leaf, direction);

                    if (
                        child.getNodeStatus().equals(SolutionNodeState.WIN) ||
                        (
                            (child.getNodeStatus().equals(SolutionNodeState.CONTINUATION) || child.getNodeStatus().equals(SolutionNodeState.REDIRECTION)) &&
                            !solutionGraph.hasNodeAtPosition(child.getPosition())
                        )
                    ) {
                        solutionGraph.addNode(child);
                        Solution partialSolution = new Solution(null, 0);
                        if (possibleSolutions.get(leaf) != null) {
                            partialSolution.getMoves().addAll(possibleSolutions.get(leaf).getMoves());
                        }
                        partialSolution.getMoves().add(new Move(leaf.getX(), leaf.getY(), child.getX(), child.getY(), direction));
                        if (child.getNodeStatus().equals(SolutionNodeState.WIN)) {
                            possibleSolutions.clear();
                            partialSolution.getMoves().add(new Move(child.getX(), child.getY(), child.getX(), child.getY(), null));
                            partialSolution.setGenerationTime(System.currentTimeMillis() - start);
                            return partialSolution;
                        } else {
                            possibleSolutions.put(child, partialSolution);
                        }
                    }
                }
                possibleSolutions.remove(leaf);
            }
        }

        return null;
    }
}
