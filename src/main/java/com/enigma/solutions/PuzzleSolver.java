package com.enigma.solutions;

import com.enigma.pojo.*;
import com.enigma.solutions.moves.*;
import org.springframework.beans.factory.annotation.*;

import java.util.*;

public abstract class PuzzleSolver {

    @Value("${solver.maxSolutionLength:10}")
    protected int maxSolutionLength;

    @Autowired
    protected MoveSimulator moveSimulator;

    public abstract Solution solvePuzzle(Puzzle puzzle);

    public Hint getHintForPuzzle(Puzzle puzzle) {
        Solution solution = this.solvePuzzle(puzzle);
        if (solution == null) {
            return null;
        }
        Hint hint = new Hint();
        hint.setMoves(new ArrayList<>(solution.getMoves().subList(0, 2)));
        return hint;
    }
}
