# Solution Graph

The solution algorithms here solve any given puzzle by treating it as a graph. The player's current position is the root-node, every node has up to 4 possible children (one for each possible movement direction). Each node has a unique ID which is the X,Y coordinate for that position in the puzzle.

Starting with the root node a BFS algorithm simulates each possible move (left, right, up, down) and if that move doesn't kill the player and puts the player in a position that doesn't already exist in the graph (ie; has a unique ID//X,Y coordinate) then it's added as a child node to the current node. All newly added leaf nodes are then processed in the same manner. Once an END or SOLUTION node is found the algorithm stops. The puzzle is now known to be solvable and the solution is any path from the root node to the END//SOLUTION node.

![Solution Graph](Solution Graph.png)
