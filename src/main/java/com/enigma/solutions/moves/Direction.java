package com.enigma.solutions.moves;

public enum Direction {
    UP, DOWN, LEFT, RIGHT
}
