package com.enigma.solutions.moves;

import com.enigma.pojo.*;
import com.enigma.solutions.graph.*;
import org.springframework.boot.autoconfigure.condition.*;
import org.springframework.stereotype.*;

@Component
@ConditionalOnProperty(name = "move-simulator.impl", havingValue = "IP")
public class ImperativeMoveSimulator implements MoveSimulator {

    public SolutionNode simulateMove(Puzzle puzzle, SolutionNode leaf, Direction direction) {
        int bounceCount = 0;

        switch (direction) {
            case UP: {
                return this.simulateMoveUp(puzzle, leaf, bounceCount);
            }
            case DOWN: {
                return this.simulateMoveDown(puzzle, leaf, bounceCount);
            }
            case LEFT: {
                return this.simulateMoveLeft(puzzle, leaf, bounceCount);
            }
            case RIGHT: {
                return this.simulateMoveRight(puzzle, leaf, bounceCount);
            }
        }

        return this.constructDeadEndNode();
    }

    private SolutionNode simulateMoveUp(Puzzle puzzle, SolutionNode leaf, int bounceCount) {
        if (bounceCount > 5) {
            SolutionNode solutionNode = new SolutionNode();
            solutionNode.setX(-1);
            solutionNode.setY(-1);
            solutionNode.setNodeStatus(SolutionNodeState.DEAD_END);
            return solutionNode;
        }

        int startingX = leaf.getX();
        int startingY = leaf.getY();

        Block hitBlock = null;
        for (int currentY = startingY + 1; currentY < puzzle.getMap().getHeight(); currentY++) {
            hitBlock = puzzle.getMap().getBlock(startingX, currentY);
            if (hitBlock != null) {
                break;
            }
        }
        if (hitBlock != null && hitBlock.getY() > (startingY + 1)) {
            if (hitBlock.getType().equals(BlockType.END)) {
                SolutionNode solutionNode = new SolutionNode();
                solutionNode.setX(hitBlock.getX());
                solutionNode.setY(hitBlock.getY());
                solutionNode.setNodeStatus(SolutionNodeState.WIN);
                return solutionNode;
            } else if (hitBlock.getType().equals(BlockType.ANGLE_0)) {
                SolutionNode fakeLeaf = new SolutionNode();
                fakeLeaf.setX(startingX);
                fakeLeaf.setY(hitBlock.getY());
                fakeLeaf.setParent(leaf.getParent());
                return this.simulateMoveRight(puzzle, fakeLeaf, bounceCount + 1);
            } else if (hitBlock.getType().equals(BlockType.ANGLE_1)) {
                SolutionNode fakeLeaf = new SolutionNode();
                fakeLeaf.setX(startingX);
                fakeLeaf.setY(hitBlock.getY());
                fakeLeaf.setParent(leaf.getParent());
                return this.simulateMoveLeft(puzzle, fakeLeaf, bounceCount + 1);
            } else if (hitBlock.getType().equals(BlockType.ANGLE_2) || hitBlock.getType().equals(BlockType.ANGLE_3) || hitBlock.getType().equals(BlockType.ROCK)) {
                if (leaf.getParent() == null || leaf.getParent().getX() != (startingX) || leaf.getParent().getY() != (hitBlock.getY() - 1)){
                    SolutionNode solutionNode = new SolutionNode();
                    solutionNode.setX(startingX);
                    solutionNode.setY(hitBlock.getY() - 1);
                    solutionNode.setNodeStatus(SolutionNodeState.CONTINUATION);
                    return solutionNode;
                }
            }
        }
        return this.constructDeadEndNode();
    }

    private SolutionNode simulateMoveDown(Puzzle puzzle, SolutionNode leaf, int bounceCount) {
        if (bounceCount > 5) {
            SolutionNode solutionNode = new SolutionNode();
            solutionNode.setX(-1);
            solutionNode.setY(-1);
            solutionNode.setNodeStatus(SolutionNodeState.DEAD_END);
            return solutionNode;
        }

        int startingX = leaf.getX();
        int startingY = leaf.getY();

        Block hitBlock = null;
        for (int currentY = startingY - 1; currentY > 0; currentY--) {
            hitBlock = puzzle.getMap().getBlock(startingX, currentY);
            if (hitBlock != null) {
                break;
            }
        }
        if (hitBlock != null && hitBlock.getY() < (startingY - 1)) {
            if (hitBlock.getType().equals(BlockType.END)) {
                SolutionNode solutionNode = new SolutionNode();
                solutionNode.setX(hitBlock.getX());
                solutionNode.setY(hitBlock.getY());
                solutionNode.setNodeStatus(SolutionNodeState.WIN);
                return solutionNode;
            } else if (hitBlock.getType().equals(BlockType.ANGLE_2)) {
                SolutionNode fakeLeaf = new SolutionNode();
                fakeLeaf.setX(startingX);
                fakeLeaf.setY(hitBlock.getY());
                fakeLeaf.setParent(leaf.getParent());
                return this.simulateMoveRight(puzzle, fakeLeaf, bounceCount + 1);
            } else if (hitBlock.getType().equals(BlockType.ANGLE_3)) {
                SolutionNode fakeLeaf = new SolutionNode();
                fakeLeaf.setX(startingX);
                fakeLeaf.setY(hitBlock.getY());
                fakeLeaf.setParent(leaf.getParent());
                return this.simulateMoveLeft(puzzle, fakeLeaf, bounceCount + 1);
            } else if (hitBlock.getType().equals(BlockType.ANGLE_0) || hitBlock.getType().equals(BlockType.ANGLE_1) || hitBlock.getType().equals(BlockType.ROCK)) {
                if (leaf.getParent() == null || leaf.getParent().getX() != (startingX) || leaf.getParent().getY() != (hitBlock.getY() + 1)) {
                    SolutionNode solutionNode = new SolutionNode();
                    solutionNode.setX(startingX);
                    solutionNode.setY(hitBlock.getY() + 1);
                    solutionNode.setNodeStatus(SolutionNodeState.CONTINUATION);
                    return solutionNode;
                }
            }
        }
        return this.constructDeadEndNode();
    }

    private SolutionNode simulateMoveLeft(Puzzle puzzle, SolutionNode leaf, int bounceCount) {
        if (bounceCount > 5) {
            SolutionNode solutionNode = new SolutionNode();
            solutionNode.setX(-1);
            solutionNode.setY(-1);
            solutionNode.setNodeStatus(SolutionNodeState.DEAD_END);
            return solutionNode;
        }

        int startingX = leaf.getX();
        int startingY = leaf.getY();

        Block hitBlock = null;
        for (int currentX = startingX - 1; currentX > 0; currentX--) {
            hitBlock = puzzle.getMap().getBlock(currentX, startingY);
            if (hitBlock != null) {
                break;
            }
        }
        if (hitBlock != null && hitBlock.getX() < (startingX - 1)) {
            if (hitBlock.getType().equals(BlockType.END)) {
                SolutionNode solutionNode = new SolutionNode();
                solutionNode.setX(hitBlock.getX());
                solutionNode.setY(hitBlock.getY());
                solutionNode.setNodeStatus(SolutionNodeState.WIN);
                return solutionNode;
            } else if (hitBlock.getType().equals(BlockType.ANGLE_0)) {
                SolutionNode fakeLeaf = new SolutionNode();
                fakeLeaf.setX(hitBlock.getX());
                fakeLeaf.setY(startingY);
                fakeLeaf.setParent(leaf.getParent());
                return this.simulateMoveDown(puzzle, fakeLeaf, bounceCount + 1);
            } else if (hitBlock.getType().equals(BlockType.ANGLE_2)) {
                SolutionNode fakeLeaf = new SolutionNode();
                fakeLeaf.setX(hitBlock.getX());
                fakeLeaf.setY(startingY);
                fakeLeaf.setParent(leaf.getParent());
                return this.simulateMoveUp(puzzle, fakeLeaf, bounceCount + 1);
            } else if (hitBlock.getType().equals(BlockType.ANGLE_1) || hitBlock.getType().equals(BlockType.ANGLE_3) || hitBlock.getType().equals(BlockType.ROCK)) {
                if (leaf.getParent() == null || leaf.getParent().getX() != (hitBlock.getX() + 1) || leaf.getParent().getY() != (startingY)) {
                    SolutionNode solutionNode = new SolutionNode();
                    solutionNode.setX(hitBlock.getX() + 1);
                    solutionNode.setY(startingY);
                    solutionNode.setNodeStatus(SolutionNodeState.CONTINUATION);
                    return solutionNode;
                }
            }
        }
        return this.constructDeadEndNode();
    }

    private SolutionNode simulateMoveRight(Puzzle puzzle, SolutionNode leaf, int bounceCount) {
        if (bounceCount > 5) {
            SolutionNode solutionNode = new SolutionNode();
            solutionNode.setX(-1);
            solutionNode.setY(-1);
            solutionNode.setNodeStatus(SolutionNodeState.DEAD_END);
            return solutionNode;
        }

        int startingX = leaf.getX();
        int startingY = leaf.getY();

        Block hitBlock = null;
        for (int currentX = startingX + 1; currentX < puzzle.getMap().getWidth(); currentX++) {
            hitBlock = puzzle.getMap().getBlock(currentX, startingY);
            if (hitBlock != null) {
                break;
            }
        }
        if (hitBlock != null && hitBlock.getX() > (startingX + 1)) {
            if (hitBlock.getType().equals(BlockType.END)) {
                SolutionNode solutionNode = new SolutionNode();
                solutionNode.setX(hitBlock.getX());
                solutionNode.setY(hitBlock.getY());
                solutionNode.setNodeStatus(SolutionNodeState.WIN);
                return solutionNode;
            } else if (hitBlock.getType().equals(BlockType.ANGLE_3)) {
                SolutionNode fakeLeaf = new SolutionNode();
                fakeLeaf.setX(hitBlock.getX());
                fakeLeaf.setY(startingY);
                fakeLeaf.setParent(leaf.getParent());
                return this.simulateMoveUp(puzzle, fakeLeaf, bounceCount + 1);
            } else if (hitBlock.getType().equals(BlockType.ANGLE_1)) {
                SolutionNode fakeLeaf = new SolutionNode();
                fakeLeaf.setX(hitBlock.getX());
                fakeLeaf.setY(startingY);
                fakeLeaf.setParent(leaf.getParent());
                return this.simulateMoveDown(puzzle, fakeLeaf, bounceCount + 1);
            } else if (hitBlock.getType().equals(BlockType.ANGLE_0) || hitBlock.getType().equals(BlockType.ANGLE_2) || hitBlock.getType().equals(BlockType.ROCK)) {
                if (leaf.getParent() == null || leaf.getParent().getX() != (hitBlock.getX() + 1) || leaf.getParent().getY() != (startingY)) {
                    SolutionNode solutionNode = new SolutionNode();
                    solutionNode.setX(hitBlock.getX() - 1);
                    solutionNode.setY(startingY);
                    solutionNode.setNodeStatus(SolutionNodeState.CONTINUATION);
                    return solutionNode;
                }
            }
        }
        return this.constructDeadEndNode();
    }

    private SolutionNode constructDeadEndNode() {
        SolutionNode solutionNode = new SolutionNode();
        solutionNode.setX(-1);
        solutionNode.setY(-1);
        solutionNode.setNodeStatus(SolutionNodeState.DEAD_END);
        return solutionNode;
    }
}
