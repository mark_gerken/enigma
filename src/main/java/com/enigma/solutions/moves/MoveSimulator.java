package com.enigma.solutions.moves;

import com.enigma.pojo.*;
import com.enigma.solutions.graph.*;

public interface MoveSimulator {
    SolutionNode simulateMove(Puzzle puzzle, SolutionNode leaf, Direction direction);
}
