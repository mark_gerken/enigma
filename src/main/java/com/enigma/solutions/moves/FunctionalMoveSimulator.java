package com.enigma.solutions.moves;

import com.enigma.pojo.*;
import com.enigma.solutions.graph.*;
import org.springframework.boot.autoconfigure.condition.*;
import org.springframework.stereotype.*;

@Component
@ConditionalOnProperty(name = "move-simulator.impl", havingValue = "FP")
public class FunctionalMoveSimulator implements MoveSimulator {

    public SolutionNode simulateMove(Puzzle puzzle, SolutionNode leaf, Direction direction) {
        try {
            return this.constructSolutionNode(this.findHitBlock(puzzle, leaf, direction), direction);
        } catch (MoveSimulationException e) {
            return this.constructDeadEndNode();
        }
    }
    
    private Block findHitBlock(Puzzle puzzle, SolutionNode leaf, Direction direction) throws MoveSimulationException {
        Coords coords = new Coords(leaf.getX(), leaf.getY());
        do {
            coords = this.simulateMoveOnce(coords, direction);
        } while(this.indexExistsAndIsEmpty(puzzle, coords));

        Block hitBlock = puzzle.getMap().getBlock(coords.x, coords.y);
        if (hitBlock == null) {
            throw new MoveSimulationException("No block found at x: " + coords.x + " y: " + coords.y + ".");
        } else {
            return hitBlock;
        }
    }
    
    private SolutionNode constructSolutionNode(Block hitBlock, Direction direction) throws MoveSimulationException {
        if (
            (BlockType.ROCK.equals(hitBlock.getType())) ||
            (hitBlock.getType().equals(BlockType.ANGLE_0) && (direction.equals(Direction.DOWN) || direction.equals(Direction.RIGHT))) ||
            (hitBlock.getType().equals(BlockType.ANGLE_1) && (direction.equals(Direction.DOWN) || direction.equals(Direction.LEFT))) ||
            (hitBlock.getType().equals(BlockType.ANGLE_2) && (direction.equals(Direction.UP) || direction.equals(Direction.RIGHT))) ||
            (hitBlock.getType().equals(BlockType.ANGLE_3) && (direction.equals(Direction.UP) || direction.equals(Direction.LEFT)))
        ) {
            return direction.equals(Direction.UP) ? new SolutionNode(hitBlock.getX(), hitBlock.getY() - 1, SolutionNodeState.CONTINUATION) :
                    direction.equals(Direction.DOWN) ? new SolutionNode(hitBlock.getX(), hitBlock.getY() + 1, SolutionNodeState.CONTINUATION) :
                    direction.equals(Direction.LEFT) ? new SolutionNode(hitBlock.getX() + 1, hitBlock.getY(), SolutionNodeState.CONTINUATION) :
                    new SolutionNode(hitBlock.getX() - 1, hitBlock.getY(), SolutionNodeState.CONTINUATION);
        } else if (BlockType.ANGLE_0.equals(hitBlock.getType())) {
            return new SolutionNode(hitBlock.getX(), hitBlock.getY(), SolutionNodeState.REDIRECTION, direction.equals(Direction.UP) ? Direction.RIGHT : Direction.DOWN);
        } else if (BlockType.ANGLE_1.equals(hitBlock.getType())) {
            return new SolutionNode(hitBlock.getX(), hitBlock.getY(), SolutionNodeState.REDIRECTION, direction.equals(Direction.UP) ? Direction.LEFT : Direction.DOWN);
        } else if (BlockType.ANGLE_2.equals(hitBlock.getType())) {
            return new SolutionNode(hitBlock.getX(), hitBlock.getY(), SolutionNodeState.REDIRECTION, direction.equals(Direction.DOWN) ? Direction.RIGHT : Direction.UP);
        } else if (BlockType.ANGLE_3.equals(hitBlock.getType())) {
            return new SolutionNode(hitBlock.getX(), hitBlock.getY(), SolutionNodeState.REDIRECTION, direction.equals(Direction.RIGHT) ? Direction.UP : Direction.LEFT);
        } else if (BlockType.PORTAL.equals(hitBlock.getType())) {
            PortalBlock portalBlock = (PortalBlock)hitBlock;
            return new SolutionNode(portalBlock.getPortalExitX(), portalBlock.getPortalExitY(), SolutionNodeState.REDIRECTION, direction);
        } else if (BlockType.END.equals(hitBlock.getType())) {
            return new SolutionNode(hitBlock.getX(), hitBlock.getY(), SolutionNodeState.WIN);
        } else {
            throw new MoveSimulationException("Can't construct SolutionNode after hitting an existing block without a valid BlockType");
        }
    }

    private boolean indexExistsAndIsEmpty(Puzzle puzzle, Coords coords) {
        return coords.x >= 0 && coords.y >= 0 && coords.x < puzzle.getMap().getWidth() && coords.y < puzzle.getMap().getHeight() && puzzle.getMap().getBlock(coords.x, coords.y) == null;
    }
    
    private Coords simulateMoveOnce(Coords coords, Direction direction) {
        if (direction.equals(Direction.UP)) {
            return new Coords(coords.x, coords.y + 1);
        } else if (direction.equals(Direction.DOWN)) {
            return new Coords(coords.x, coords.y - 1);
        } else if (direction.equals(Direction.LEFT)) {
            return new Coords(coords.x - 1, coords.y);
        } else {
            return new Coords(coords.x + 1, coords.y);
        }
    }

    private SolutionNode constructDeadEndNode() {
        return new SolutionNode(-1, -1, SolutionNodeState.DEAD_END);
    }

    private class MoveSimulationException extends Exception {
        MoveSimulationException(String message) {
            super(message);
        }
    }
    
    private class Coords {
        private int x = -1;
        private int y = -1;

        Coords(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }
    }
}
