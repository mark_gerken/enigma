package com.enigma.solutions.graph;

import java.util.*;

public class PuzzleSolutionGraph {
    private SolutionNode root;
    private HashMap<String, SolutionNode> existingNodes = new HashMap<>();
    private ArrayList<SolutionNode> newLeafNodes = new ArrayList<>();

    public PuzzleSolutionGraph(int rootXIndex, int rootYIndex) {
        this.root = new SolutionNode(rootXIndex, rootYIndex, SolutionNodeState.CONTINUATION);
        this.existingNodes.put(this.root.getPosition(), this.root);
        this.newLeafNodes.add(this.root);
    }

    public ArrayList<SolutionNode> getLeafNodes() {
        ArrayList<SolutionNode> leafNodes = new ArrayList<>(this.newLeafNodes);
        this.newLeafNodes.clear();
        return leafNodes;
    }

    public boolean hasNewLeafNodes() {
        return this.newLeafNodes.size() > 0;
    }

    public void addNode(SolutionNode node) {
        this.existingNodes.put(node.getPosition(), node);
        this.newLeafNodes.add(node);
    }

    public boolean hasNodeAtPosition(String position) {
        return this.existingNodes.get(position) != null;
    }
}
