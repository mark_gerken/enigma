package com.enigma.solutions.graph;

public enum SolutionNodeState {
    WIN,
    DEAD_END,
    CONTINUATION,
    REDIRECTION
}
