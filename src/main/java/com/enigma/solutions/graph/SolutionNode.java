package com.enigma.solutions.graph;

import com.enigma.solutions.moves.*;

import java.util.*;

public class SolutionNode {
    private int x;
    private int y;
    private SolutionNode parent = null;
    private Direction newDirection = null;
    private SolutionNodeState nodeStatus;
    private final Map<Direction, SolutionNode> children = new HashMap<>();

    public SolutionNode() {
    }

    public SolutionNode(int x, int y, SolutionNodeState state) {
        this.x = x;
        this.y = y;
        this.nodeStatus = state;
    }

    public SolutionNode(int x, int y, SolutionNodeState state, Direction newDirection) {
        this(x, y, state);
        this.newDirection = newDirection;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public SolutionNodeState getNodeStatus() {
        return nodeStatus;
    }

    public void setNodeStatus(SolutionNodeState nodeStatus) {
        this.nodeStatus = nodeStatus;
    }

    public Map<Direction, SolutionNode> getChildren() {
        return children;
    }

    public SolutionNode getParent() {
        return parent;
    }

    public void setParent(SolutionNode parent) {
        this.parent = parent;
    }

    public Direction getNewDirection() {
        return newDirection;
    }

    public void setNewDirection(Direction newDirection) {
        this.newDirection = newDirection;
    }

    public String getPosition() {
        return this.x + "-" + this.y;
    }
}
