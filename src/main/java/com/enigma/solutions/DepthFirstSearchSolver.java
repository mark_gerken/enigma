package com.enigma.solutions;

import com.enigma.pojo.*;
import org.springframework.boot.autoconfigure.condition.*;
import org.springframework.stereotype.*;

import java.util.*;

@Component
@ConditionalOnProperty(name = "solver.impl", havingValue = "DFS")
public class DepthFirstSearchSolver  extends PuzzleSolver {
    @Override
    public Solution solvePuzzle(Puzzle puzzle) {
//        long start = System.currentTimeMillis();
//        ArrayList<Block> playerBlocks = puzzle.getMap().getBlocks().stream().filter(block -> block.getType().equals(BlockType.PLAYER)).collect(Collectors.toCollection(ArrayList::new));
//        ArrayList<Block> winBlocks = puzzle.getMap().getBlocks().stream().filter(block -> block.getType().equals(BlockType.END)).collect(Collectors.toCollection(ArrayList::new));
//        if (playerBlocks.size() != 1 || winBlocks.size() != 1) {
//            return null;
//        }
//        Block playerBlock = playerBlocks.get(0);
//        PuzzleSolutionGraph solutionGraph  = new PuzzleSolutionGraph(playerBlock.getX(), playerBlock.getY());
//
//        for (int i = 0; i < this.maxSolutionLength; i++) {
//            // todo; we don't care about leaf nodes here as we'll track down the leaf nodes ourselves;
//            //  our algo should be recursive at this point;
//            //  grab all leaf nodes (first as a seed) or the root directly
//            //  go in every diraction
//            //  when we hit a real node; recursively use it as the seed and go in every direction
//            //  eventually we hit the win block of we return all the way back up the stack with a null value to the seed node
//            //  eventually either a solution is found or we get nothing and the solution isn't possible
//        }

        // we couldn't find a solution
        return null;
    }
}