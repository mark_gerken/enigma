package com.enigma;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.*;

@SpringBootApplication
public class EnigmaApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(EnigmaApplication.class, args);
    }

}
