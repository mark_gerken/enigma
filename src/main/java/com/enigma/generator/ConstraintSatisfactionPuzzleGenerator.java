package com.enigma.generator;

import com.enigma.pojo.*;
import org.springframework.boot.autoconfigure.condition.*;
import org.springframework.stereotype.*;

@Component
@ConditionalOnProperty(name = "generator.impl", havingValue = "CS")
public class ConstraintSatisfactionPuzzleGenerator  implements PuzzleGenerator {


    /*
    *
    * X = {X1, X2, X3... Xn) = the set of variables
    * D = {D1, D2, D3... Dn} = the domain of possible values for the set of variables
    * C = {C1, C2, C3... Cn} = the constraints
    *
    *
    * X = {number-of-blocks, number-of-moves-to-solve, number-of-angled-blocks}
    * D = {>= .1 * map-size, >= 5, >= 0.01 * map-size}
    * C = {number-of-blocks >= 0.1 * map-size, number-of-moves-to-solve >- 5, number-of-angled-blocks >= 0.01 * map-size}
    *
    *
    *
    * */

    @Override
    public Puzzle generatePuzzle(int width, int height, Difficulty difficulty) {
        // todo; implement this
        return null;
    }
}

