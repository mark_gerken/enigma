package com.enigma.generator;

import com.enigma.pojo.Map;
import com.enigma.pojo.*;
import com.enigma.solutions.*;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.autoconfigure.condition.*;
import org.springframework.stereotype.*;

import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.stream.*;

@Component
@ConditionalOnProperty(name = "generator.impl", havingValue = "GA")
public class GeneticPuzzleGenerator implements PuzzleGenerator {
    org.slf4j.Logger logger = LoggerFactory.getLogger(GeneticPuzzleGenerator.class);

    @Value("${generator.genetic.generationCount:100}")
    private int generationCount;

    @Value("${generator.genetic.seedPopulationSize:10}")
    private int seedPopulationSize;

    @Value("${generator.genetic.mutationRate:0.2}")
    private double mutationRate;

    @Autowired
    private PuzzleSolver puzzleSolver;

    @Override
    public Puzzle generatePuzzle(int width, int height, Difficulty difficulty) {
        return this.generatePuzzleImpl(width, height, difficulty);
    }

    private Puzzle generatePuzzleImpl(int width, int height, Difficulty difficulty) {
        long startTime = System.currentTimeMillis();

        ArrayList<Callable<Pair<Puzzle, Integer>>> islandRunnables = new ArrayList<Callable<Pair<Puzzle, Integer>>>(this.seedPopulationSize);
        Pair<Puzzle, Integer>[] islandSurvivors = new Pair[this.seedPopulationSize];
        for (int i = 0; i < this.seedPopulationSize; i++) {
            int index = i;
            islandRunnables.add(() -> islandSurvivors[index] = sequentialGeneticPuzzleGeneration(width, height, difficulty));
        }

        try {
            ExecutorService executorService = Executors.newFixedThreadPool(this.seedPopulationSize);
            executorService.invokeAll(islandRunnables);

            Arrays.sort(islandSurvivors, Comparator.comparing((Pair<Puzzle, Integer> individualOne) -> individualOne.second).reversed());

            long puzzleGenerationTime = (System.currentTimeMillis() - startTime);
            Puzzle bestPuzzle = islandSurvivors[0].first;
            this.logger.info("GA - {}", puzzleGenerationTime);
            this.logger.info("Difficulty: {} SolutionLength: {}", difficulty, bestPuzzle.getPar());
            bestPuzzle.setGenerationTime(puzzleGenerationTime);
            return bestPuzzle;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Pair<Puzzle, Integer> sequentialGeneticPuzzleGeneration(int width, int height, Difficulty difficulty) {
        ExecutorService executorService = Executors.newFixedThreadPool(4);
        ArrayList<Pair<Puzzle, Integer>> population = new ArrayList<>(this.seedPopulationSize * 10);
        Function<Puzzle, Integer> fitnessCalculator = this.fitnessCalculatorByDifficulty.apply(difficulty);

        Function<
                ArrayList<Pair<Puzzle, Integer>>,
                Function<Double, Puzzle>
                > getParentFromPopulation = pop -> random -> {
            return pop.get((int) (random * pop.size())).first;
        };

        Function<Double, Puzzle> getParent = getParentFromPopulation.apply(population);

        Function<
                Pair<Puzzle, Integer>[],
                Function<
                        Integer,
                        Callable<Pair<Puzzle, Integer>>
                >
         > getCallable = newGeneration -> index -> {
            return () -> {
                Puzzle mom = getParent.apply(Math.random());
                Puzzle dad = getParent.apply(Math.random());
                Puzzle child = this.breadIndividuals(mom, dad, difficulty);
                newGeneration[index] = new Pair<Puzzle, Integer>(child, fitnessCalculator.apply(child));
                return null;
            };
        };

        for (int generation = 1; (generation < this.generationCount || population.size() <= 0); generation++) {
            while (population.size() < this.seedPopulationSize) {
                Puzzle newIndividual = this.createSimplePuzzle(width, height);
                this.mutateIndividual(newIndividual, difficulty, generation * 3);
                population.add(new Pair<Puzzle, Integer>(newIndividual, fitnessCalculator.apply(newIndividual)));
            }
            Pair<Puzzle, Integer>[] newGeneration = new Pair[this.seedPopulationSize * 11];
            Function<Integer, Callable<Pair<Puzzle, Integer>>> thisGenerationsFitnessCallable = getCallable.apply(newGeneration);
            ArrayList<Callable<Pair<Puzzle, Integer>>> fitnessRunnables = new ArrayList<Callable<Pair<Puzzle, Integer>>>(this.seedPopulationSize * 10);
            for (int i = 0; i < this.seedPopulationSize * 10; i++) {
                fitnessRunnables.add(thisGenerationsFitnessCallable.apply(i));
            }

            try {
                executorService.invokeAll(fitnessRunnables);
                for (int i = 0; i < this.seedPopulationSize; i++) {
                    newGeneration[(this.seedPopulationSize * 10) + i] = population.get(i);
                }

                population = Arrays.stream(newGeneration)
                        .filter((Pair<Puzzle, Integer> individual) -> individual.second > 0)
                        .sorted(Comparator.comparing((Pair<Puzzle, Integer> individualOne) -> individualOne.second).reversed())
                        .limit(this.seedPopulationSize)
                        .collect(Collectors.toCollection(ArrayList::new));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // at the end of N generations take the best puzzle and return it
        return population.get(0);
    }

    // todo; semantically rename everything below to clear-up what is happening
    private final Function<
            Integer,
            Function<
                    Function<Integer, Integer>,
                    Function<
                            Function<Integer, Integer>,
                            Function<Puzzle, Integer>
                    >
            >
    > fitnessEvaluator = parFitness -> blockFitnessCalculator -> isoBlockFitnessCalculator -> puzzle -> {
        int totalNumberOfBlocks = puzzle.getMap().getHeight() * puzzle.getMap().getWidth();

        int targetBlockCount = blockFitnessCalculator.apply(totalNumberOfBlocks);
        int numIsolatedBlocksFromTarget = isoBlockFitnessCalculator.apply(targetBlockCount);

        int numBlocksFromTarget = Math.abs(targetBlockCount - puzzle.getMap().getBlockCount());
        int blockFitness = (int) (200 * (((double) totalNumberOfBlocks - (double) numBlocksFromTarget) / (double) totalNumberOfBlocks));
        int blockPositionFitness = (int) (200 * (((double) totalNumberOfBlocks - (double) numIsolatedBlocksFromTarget) / (double) totalNumberOfBlocks));

        return parFitness + blockFitness + blockPositionFitness;
    };

    private final Function<
            Function<Puzzle, Function<Integer, Integer>>,
            Function<Puzzle, Integer>
    > fitnessCalculator = evaluator -> puzzle -> {
        Solution solution = this.puzzleSolver.solvePuzzle(puzzle);
        if (solution != null) {
            puzzle.setPar(solution.getMoves().size() - 1);
            int numberOfIsolatedBlocks = puzzle.getMap().getNumberOfIsolatedBlocks();
            return evaluator.apply(puzzle).apply(numberOfIsolatedBlocks);
        } else {
            return -1;
        }
    };

    private final Function<Difficulty, Function<Puzzle, Integer>> fitnessCalculatorByDifficulty = difficulty -> {
        if (difficulty.equals(Difficulty.EASY)) {
            return fitnessCalculator.apply(puzzle -> numberOfIsolatedBlocks -> {
                return fitnessEvaluator
                        .apply(puzzle.getPar() <= 6 ? puzzle.getPar() : -1 * puzzle.getPar())
                        .apply((blockCount) -> (int)(blockCount * -0.06))
                        .apply((targetBlockCount) -> Math.abs((targetBlockCount / 10) - numberOfIsolatedBlocks))
                        .apply(puzzle);
            });
        } else if (difficulty.equals(Difficulty.MEDIUM)) {
            return fitnessCalculator.apply(puzzle -> numberOfIsolatedBlocks -> {
                return fitnessEvaluator
                        .apply(puzzle.getPar() <= 15 ? puzzle.getPar() : -1 * puzzle.getPar())
                        .apply((blockCount) -> (int)(blockCount * -0.01))
                        .apply((targetBlockCount) -> Math.abs((targetBlockCount / 5) - numberOfIsolatedBlocks))
                        .apply(puzzle);
            });
        } else {
            return fitnessCalculator.apply(puzzle -> numberOfIsolatedBlocks -> {
                return fitnessEvaluator
                        .apply(puzzle.getPar() * 100)
                        .apply((blockCount) -> (int)(blockCount * -0.03))
                        .apply((targetBlockCount) -> 0)
                        .apply(puzzle);
            });
        }
    };

    private Puzzle breadIndividuals(Puzzle mom, Puzzle dad, Difficulty difficulty) {
        Puzzle child = new Puzzle(new Map(mom.getMap().getWidth(), mom.getMap().getHeight()), -1, -1);
        int splitIndex = (int)(Math.random() * mom.getMap().getHeight());

        for (int y = 0; y < mom.getMap().getHeight(); y++) {
            child.getMap().setRow(y, (y < splitIndex ? mom : dad).getMap().getRow(y));
        }

        this.mutateIndividual(child, difficulty);
        return child;
    }

    private void mutateIndividual(Puzzle individual, Difficulty difficulty, Integer count) {
        for (int i = 0; i < count; i++) {
            this.mutateIndividual(individual, difficulty);
        }
    }

    private void mutateIndividual(Puzzle individual, Difficulty difficulty) {
        int mutationAmount = (int) (individual.getMap().getWidth() * individual.getMap().getHeight() * this.mutationRate);
        for (int i = 0; i < mutationAmount; i++) {
            int x = (int)(Math.random() * individual.getMap().getWidth());
            int y = (int)(Math.random() * individual.getMap().getHeight());

            Block existingBlock = individual.getMap().getBlock(x, y);

            if (existingBlock == null) {
                int seed = (int)(Math.random() * 100);
                if (difficulty.equals(Difficulty.HARD) && seed <= 1) {
                    Block portalExist;
                    int x1;
                    int y1;
                    do {
                        x1 = (int)(Math.random() * individual.getMap().getWidth());
                        y1 = (int)(Math.random() * individual.getMap().getHeight());
                        portalExist = individual.getMap().getBlock(x1, y1);
                    } while (portalExist != null);

                    PortalBlock portalBlock = new PortalBlock(x, y, x1, y1, x + ":" + y + ":" + x1, individual.getMap().getWidth(), individual.getMap().getHeight());
                    PortalBlock portalBlockExit = new PortalBlock(x1, y1, x, y, x + ":" + y + ":" + x1, individual.getMap().getWidth(), individual.getMap().getHeight());
                    
                    individual.getMap().addBlock(portalBlock);
                    individual.getMap().addBlock(portalBlockExit);
                } else if ((difficulty.equals(Difficulty.HARD) || difficulty.equals(Difficulty.MEDIUM)) && seed <= 14) {
                    individual.getMap().addBlock(new Block(x, y, BlockType.valueOf("ANGLE_" + ((int) (Math.random() * 4)))));
                } else {
                    individual.getMap().addBlock(new Block(x, y, BlockType.ROCK));
                }
            } else if (!existingBlock.getType().equals(BlockType.PLAYER) && !existingBlock.getType().equals(BlockType.PLAYER_1) && !existingBlock.getType().equals(BlockType.END)) {
                if (existingBlock.getType().equals(BlockType.PORTAL)) {
                    PortalBlock portalBlock = (PortalBlock)existingBlock;
                    Block exitBlock = individual.getMap().getBlock(portalBlock.getPortalExitX(), portalBlock.getPortalExitY());
                    if (exitBlock != null) {
                        individual.getMap().removeBlock(exitBlock);
                    }
                }
                individual.getMap().removeBlock(existingBlock);
            }
        }
    }

    private Puzzle createSimplePuzzle(int width, int height) {
        Puzzle puzzle = new Puzzle(new Map(width, height), -1, -1);
        Map map = puzzle.getMap();

        int x = (int)(Math.random() * map.getWidth());
        int y = (int)(Math.random() * map.getHeight());
        Block endBlock = new Block(x, y, BlockType.END);
        map.addBlock(endBlock);

        int newX;
        do {
            newX = (int)(Math.random() * map.getWidth());
        } while (newX == x);
        int newY;
        do {
            newY = (int)(Math.random() * map.getHeight());
        } while (newY == y);

        Block playerBlock = new Block(newX, newY, BlockType.PLAYER);
        map.addBlock(playerBlock);

        return puzzle;
    }
}
