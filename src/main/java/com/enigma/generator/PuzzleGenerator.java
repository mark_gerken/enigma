package com.enigma.generator;

import com.enigma.pojo.*;

public interface PuzzleGenerator {
    public Puzzle generatePuzzle(int width, int height, Difficulty difficulty);
}
