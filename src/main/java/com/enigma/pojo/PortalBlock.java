package com.enigma.pojo;

public class PortalBlock extends Block {
    private int portalExitX;
    private int portalExitY;
    private String portalId;
    private int red;
    private int green;
    private int blue;

    public PortalBlock() {
        super();
    }

    public PortalBlock(int x, int y, int exitX, int exitY, String id, int width, int height) {
        super(x, y, BlockType.PORTAL);
        this.portalExitX = exitX;
        this.portalExitY = exitY;
        this.portalId = id;

        this.red = (int)((Double.parseDouble(id.split(":")[0]) / width) * 255);
        this.green = (int)((Double.parseDouble(id.split(":")[1]) / height) * 255);
        this.blue = (int)((Double.parseDouble(id.split(":")[2]) / width) * 255);
    }

    public void setPortalId(String id) {
        this.portalId = id;
    }

    public String getPortalId() {
        return this.portalId;
    }

    public void setPortalExitX(int x) {
        this.portalExitX = x;
    }

    public void setPortalExitY(int y) {
        this.portalExitY = y;
    }

    public int getPortalExitX() {
        return this.portalExitX;
    }

    public int getPortalExitY() {
        return this.portalExitY;
    }

    public int getRed() {
        return this.red;
    }

    public void setRed(int red) {
        this.red = red;
    }
    
    public int getGreen() {
        return this.green;
    }

    public void setGreen(int green) {
        this.green = green;
    }
    
    public int getBlue() {
        return this.blue;
    }

    public void setBlue(int blue) {
        this.blue = blue;
    }
}
