package com.enigma.pojo;

public enum BlockType {
    ANGLE_0,
    ANGLE_1,
    ANGLE_2,
    ANGLE_3,
    ROCK,
    END,
    PLAYER,
    PLAYER_1,
    PORTAL
}
