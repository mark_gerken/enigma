package com.enigma.pojo;

import com.enigma.solutions.moves.*;

public class Move {
    private final int x;
    private final int y;
    private final Direction direction;
    private final int x1;
    private final int y1;

    public Move(int x, int y, int x1, int y1, Direction direction) {
        this.x = x;
        this.y = y;
        this.x1 = x1;
        this.y1 = y1;
        this.direction = direction;
    }

    public Direction getDirection() {
        return direction;
    }

    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }

    public int getX1() {
        return x1;
    }

    public int getY1() {
        return y1;
    }
}
