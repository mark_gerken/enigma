package com.enigma.pojo;

public class Puzzle {

	private int par;
	private Map map;
	private long generationTime = 0;

	public Puzzle(Map map, int par, long generationTime) {
		this.map = map;
		this.par = par;
		this.generationTime = generationTime;
	}

	public Map getMap() {
		return map;
	}

	public void setMap(Map map) {
		this.map = map;
	}

	public int getPar() {
		return par;
	}

	public void setPar(int par) {
		this.par = par;
	}

	public long getGenerationTime() {
		return generationTime;
	}

	public void setGenerationTime(long generationTime) {
		this.generationTime = generationTime;
	}
}
