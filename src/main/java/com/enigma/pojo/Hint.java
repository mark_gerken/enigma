package com.enigma.pojo;

import java.util.*;

public class Hint {
    private ArrayList<Move> moves;

    public Hint() {
        this.moves = new ArrayList<>();
    }

    public ArrayList<Move> getMoves() {
        return moves;
    }

    public void setMoves(ArrayList<Move> moves) {
        this.moves = moves;
    }
}
