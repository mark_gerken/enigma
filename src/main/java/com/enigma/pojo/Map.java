package com.enigma.pojo;

import java.util.*;

public class Map {
    private int width;
    private int height;
    private java.util.Map<Integer, java.util.Map<Integer, Block>> blockMap;
    private ArrayList<Block> playerBlocks;
    private ArrayList<Block> endBlocks;

    public Map(int width, int height) {
        this.width = width;
        this.height = height;
        this.blockMap = new HashMap<Integer, java.util.Map<Integer, Block>>();
        this.playerBlocks = new ArrayList<>();
        this.endBlocks = new ArrayList<>();
    }

    public void addBlock(Block block) {
        java.util.Map<Integer, Block> xAxisBlocks = this.blockMap.getOrDefault(block.getY(), new HashMap<>());
        xAxisBlocks.put(block.getX(), block);
        this.blockMap.put(block.getY(), xAxisBlocks);
        if (block.getType().equals(BlockType.PLAYER) || block.getType().equals(BlockType.PLAYER_1)) { playerBlocks.add(block); }
        if (block.getType().equals(BlockType.END)) { endBlocks.add(block); }
    }

    public void removeBlock(Block block) {
        this.blockMap.get(block.getY()).remove(block.getX());
        if (block.getType().equals(BlockType.PLAYER) || block.getType().equals(BlockType.PLAYER_1)) { playerBlocks.remove(block); }
        if (block.getType().equals(BlockType.END)) { endBlocks.remove(block); }
    }

    public java.util.Map<Integer, Block> getRow(int y) {
        return this.blockMap.get(y);
    }

    public void setRow(Integer y, java.util.Map<Integer, Block> row) {
        if (row != null) {
            this.blockMap.put(y, new HashMap<>(row));
        }
    }

    public Block getBlock(int x, int y) {
        return this.blockMap.getOrDefault(y, Collections.emptyMap()).get(x);
    }

    public int getBlockCount() {
        return blockMap.values().stream().map(xValues -> xValues.keySet().size()).reduce(0, Integer::sum);
    }

    public Integer getNumberOfIsolatedBlocks() {
        return blockMap.values().stream().map(xValues -> xValues.values().stream().filter(this::isBlockIsolated).mapToInt(a -> 1).sum()).reduce(0, Integer::sum);
    }

    public ArrayList<Block> getPlayerBlocks() {
        return playerBlocks;
    }

    public ArrayList<Block> getEndBlocks() {
        return endBlocks;
    }

    public java.util.Map<Integer, java.util.Map<Integer, Block>> getBlocks() { return this.blockMap; }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    private boolean isBlockIsolated(Block block) {
        return blockMap.get(block.getY()).get(block.getX() + 1) == null &&
                blockMap.get(block.getY()).get(block.getX() + 1) == null &&
                blockMap.getOrDefault(block.getY() + 1, Collections.emptyMap()).get(block.getX()) == null &&
                blockMap.getOrDefault(block.getY() - 1, Collections.emptyMap()).get(block.getX()) == null;
    }
}
