package com.enigma.pojo;

import java.util.*;

public class Solution {
    private final ArrayList<Move> moves;
    private long generationTime = 0;

    public Solution(ArrayList<Move> moves, long generationTime) {
        this.moves = moves == null ? new ArrayList<>() : moves;
        this.generationTime = generationTime;
    }

    public ArrayList<Move> getMoves() {
        return moves;
    }

    public long getGenerationTime() {
        return generationTime;
    }

    public void setGenerationTime(long generationTime) {
        this.generationTime = generationTime;
    }
}
