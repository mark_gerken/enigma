package com.enigma.pojo;

public enum Difficulty {
    EASY,
    MEDIUM,
    HARD
}
