package com.enigma.api;

import com.enigma.generator.*;
import com.enigma.pojo.*;
import com.enigma.solutions.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.nio.charset.*;
import java.nio.file.*;
import java.util.stream.*;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:3000")
public class PuzzleController {
	@Autowired
	private PuzzleGenerator generator;

	@Autowired
	private PuzzleSolver solver;

	@GetMapping(value = "/performance", produces = "application/json")
	public String getPerformanceStats() {
		StringBuilder contentBuilder = new StringBuilder();

		try (Stream<String> stream = Files.lines(Paths.get("./newman-raw-output.json"), StandardCharsets.UTF_8)) {
			stream.forEach(contentBuilder::append);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return contentBuilder.toString();
	}

	@GetMapping(value = "/puzzle", produces = "application/json")
	public Puzzle generateNewPuzzle(
		@RequestParam(value = "width", defaultValue = "20") int width,
		@RequestParam(value = "height", defaultValue = "20" ) int height,
		@RequestParam(value = "difficulty", defaultValue = "EASY") Difficulty difficulty
	) {
		return this.generator.generatePuzzle(width, height, difficulty);
	}

	@PostMapping(value = "/solution", produces = "application/json")
	public Solution solution(
		@RequestBody() Puzzle puzzle
	) {
		return this.solver.solvePuzzle(puzzle);
	}

	@PostMapping(value = "/hint", produces = "application/json")
	public Hint hint(
		@RequestBody() Puzzle puzzle
	) {
		return this.solver.getHintForPuzzle(puzzle);
	}
}
