package com.enigma.solutions;

import com.enigma.generator.*;
import com.enigma.pojo.*;
import com.enigma.solutions.graph.*;
import com.enigma.solutions.moves.*;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.autoconfigure.web.servlet.*;
import org.springframework.boot.test.context.*;


@SpringBootTest
@AutoConfigureMockMvc
public class MoveSimulatorTests {
    @Autowired
    private FunctionalMoveSimulator functionalMoveSimulator;

    @Autowired
    private PuzzleGenerator generator;

    @Test
    public void requestPuzzleAndSolvePuzzle() throws Exception {
        Puzzle puzzle = generator.generatePuzzle(100, 100, Difficulty.HARD);
        Block playerBlock = puzzle.getMap().getPlayerBlocks().get(0);
        assert(functionalMoveSimulator.simulateMove(puzzle, new SolutionNode(playerBlock.getX(), playerBlock.getY(), SolutionNodeState.CONTINUATION), Direction.LEFT) != null);
        assert(functionalMoveSimulator.simulateMove(puzzle, new SolutionNode(playerBlock.getX(), playerBlock.getY(), SolutionNodeState.CONTINUATION), Direction.RIGHT) != null);
        assert(functionalMoveSimulator.simulateMove(puzzle, new SolutionNode(playerBlock.getX(), playerBlock.getY(), SolutionNodeState.CONTINUATION), Direction.UP) != null);
        assert(functionalMoveSimulator.simulateMove(puzzle, new SolutionNode(playerBlock.getX(), playerBlock.getY(), SolutionNodeState.CONTINUATION), Direction.DOWN) != null);
    }
}
