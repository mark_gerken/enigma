/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	  https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.enigma.api;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.autoconfigure.web.servlet.*;
import org.springframework.boot.test.context.*;
import org.springframework.mock.web.*;
import org.springframework.test.web.servlet.*;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class PuzzleControllerTests {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void requestPuzzle() throws Exception {
		this.mockMvc.perform(get("/api/puzzle")).andDo(print()).andExpect(status().isOk());
	}

	@Test
	public void requestPuzzleWithSpecifiedDimensions() throws Exception {
		this.mockMvc.perform(get("/api/puzzle?width=100&height=27")).andDo(print()).andExpect(status().isOk());
	}

	@Test
	public void requestPuzzleAndSolvePuzzle() throws Exception {
		MockHttpServletResponse puzzleResponse = this.mockMvc.perform(get("/api/puzzle?width=100&height=100&difficulty=HARD")).andReturn().getResponse();

		this.mockMvc.perform(post("/api/solution")
				.contentType(APPLICATION_JSON)
				.content(puzzleResponse.getContentAsString()))
				.andExpect(status().isOk());
	}
}
