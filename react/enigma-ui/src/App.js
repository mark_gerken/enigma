import React, { Component } from 'react';
import './App.css';
import Board from './components/puzzle/Board';
import Menu from './components/Menu';
import { connect } from 'react-redux';
import { movePlayerBlockThunk } from './redux/actions/movePlayerBlockThunk';
import { userDiedThunk } from './redux/actions/userDied';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Alert from '@material-ui/lab/Alert';
import Button from '@material-ui/core/Button';

const styles = (theme) => ({
    alertToast: {
        transform: 'translateY(-85vh)',
        margin: 'auto',
        marginTop: '5px',
        width: '80%',
        zIndex: 9001,
        '&>.message': {
            width: '100%'
        }
    }
});

class App extends Component {
    lastDownX = -1;
    lastDownY = -1;

    themes = {
        LIGHT: 0,
        DARK: 1
    };

    theme = this.themes.DARK;

    // todo; move this into separate css variable files which are required here
    themeCss = {
        [this.themes.LIGHT]: {
            '--app-background-color': 'slategrey',
            '--app-border-color': 'black',

            '--board-square-border': 'white',
            '--board-square-background': 'lightsteelblue',
            '--board-square-rock-background': 'lightgrey',
            '--board-square-rock-hit-background': '#b37400',
            '--board-square-rock-hit-border': 'orange',

            '--board-square-player-background': 'orange',
            '--board-square-player-border-color': 'black',
            '--board-square-player-1-background': 'deepskyblue',
            '--board-square-player-1-border-color': 'black',
            '--board-square-hint-background': 'darkgoldenrod',
            '--board-square-angle-background': 'lightskyblue',

            '--menu-bar-background-color': 'peru',
            '--menu-bar-font-color': 'burlywood',
            '--menu-bar-item-selected': 'white',

            '--board-square-end-background': 'black',
            '--board-square-end-border-color': 'black',

            '--board-square-end-opacity': '0.6',
            '--board-square-end-padding': '0px',
            '--board-square-end-margin': '2px',
            '--board-square-end-width': '20px',
            '--board-square-end-height': '20px',

            '--modal-cancel-button-text-color': 'darkred',
            '--modal-ready-button-text-color': 'greenyellow'
        },
        [this.themes.DARK]: {
            '--app-background-color': '#202020',
            '--app-border-color': 'black',

            '--board-square-border': '#555',
            '--board-square-background': '#303030',
            '--board-square-rock-background': '#404040',
            '--board-square-rock-hit-background': '#035987',
            '--board-square-rock-hit-border': 'steelblue',

            '--board-square-player-1-background': 'orange',
            '--board-square-player-1-border-color': 'black',
            '--board-square-player-background': 'deepskyblue',
            '--board-square-player-border-color': 'black',
            '--board-square-hint-background': 'steelblue',
            '--board-square-angle-background': 'cornflowerblue',

            '--menu-bar-background-color': 'royalblue',
            '--menu-bar-font-color': 'cornflowerblue',
            '--menu-bar-item-selected': 'white',

            '--board-square-end-background': 'darkorchid',

            '--board-square-end-border-color': 'orchid',
            '--board-square-end-opacity': '0.6',
            '--board-square-end-padding': '0px',
            '--board-square-end-margin': '2px',
            '--board-square-end-width': '20px',
            '--board-square-end-height': '20px',

            '--modal-cancel-button-text-color': 'pink',
            '--modal-ready-button-text-color': 'greenyellow'
        }
    };

    constructor(props) {
        super();
        this._handleMouseUp = this._handleMouseUp.bind(this);
    }

    componentWillUnmount() {
        document.removeEventListener('keydown', this._handleKeyDown);
    }

    componentDidMount() {
        document.addEventListener('keydown', this._handleKeyDown.bind(this));
        var newCssValues = this.themeCss[this.theme];
        Object.keys(newCssValues).forEach((cssStyleAttr) => {
            document.body.style.setProperty(cssStyleAttr, newCssValues[cssStyleAttr]);
        });
    }

    render() {
        const { classes } = this.props;
        return (
            <Router>
                <div className={'App'}>
                    <Switch>
                        <Route path="/">
                            <div className="boardWrapper" data-testid="boardWrapper" onTouchStart={this._handleMouseDown.bind(this)}>
                                <Board matchId={null}></Board>
                            </div>
                        </Route>
                    </Switch>
                    {Object.keys(this.props.alertMessage).map((key, index) => {
                        return (
                            <Alert
                                severity={this.props.alertMessage[key].severity}
                                className={'visibleAlertMessage ' + classes.alertToast}
                                key={key}
                                action={
                                    this.props.alertMessage[key].includeRestartOption ? (
                                        <Button
                                            data-testid="removeAllAlertsButton"
                                            color="inherit"
                                            size="small"
                                            onClick={() => {
                                                this.props.dispatch({ type: 'REMOVE_ALL_ALERT_MESSAGES' });
                                                this.props.dispatch(userDiedThunk());
                                            }}
                                        >
                                            RESTART
                                        </Button>
                                    ) : null
                                }
                                onClose={() => {
                                    this.props.dispatch({ type: 'REMOVE_ALERT_MESSAGE', payload: { id: key } });
                                }}
                            >
                                {this.props.alertMessage[key].message} <span dangerouslySetInnerHTML={{ __html: this.props.alertMessage[key].html }}></span>
                            </Alert>
                        );
                    })}
                    <Menu
                        toggleThemeCallback={() => {
                            this._toggleTheme(this.theme === this.themes.DARK ? 'LIGHT' : 'DARK');
                        }}
                        className="menuThingy"
                        difficulty={this.props.difficulty}
                        dispatch={this.props.dispatch}
                        currentTheme={this.theme}
                    ></Menu>
                </div>
            </Router>
        );
    }

    // todo; move this to be something that is stored in the redux-store
    _toggleTheme(themeColor) {
        if (themeColor === 'LIGHT') {
            this.theme = this.themes.LIGHT;
        } else if (themeColor === 'DARK') {
            this.theme = this.themes.DARK;
        }
        var newCssValues = this.themeCss[this.theme];
        Object.keys(newCssValues).forEach((cssStyleAttr) => {
            document.body.style.setProperty(cssStyleAttr, newCssValues[cssStyleAttr]);
        });
    }

    _handleMouseDown(event) {
        window.addEventListener('touchend', this._handleMouseUp);
        event.stopPropagation();
        this.lastDownX = event.changedTouches[0].clientX;
        this.lastDownY = event.changedTouches[0].clientY;
    }

    _handleMouseUp = (event) => {
        window.removeEventListener('touchend', this._handleMouseUp);
        event.stopPropagation();
        event.preventDefault();
        var currentX = event.changedTouches[0].clientX;
        var currentY = event.changedTouches[0].clientY;

        var leftRight = Math.abs(this.lastDownX - currentX) > Math.abs(this.lastDownY - currentY);
        if (leftRight) {
            if (this.lastDownX < currentX) {
                this._moveRight();
            } else {
                this._moveLeft();
            }
        } else {
            if (this.lastDownY < currentY) {
                this._moveDown();
            } else {
                this._moveUp();
            }
        }
    };

    _handleKeyDown(event) {
        switch (event.code) {
            case 'ArrowLeft': {
                this._moveLeft();
                break;
            }
            case 'ArrowRight': {
                this._moveRight();
                break;
            }
            case 'ArrowDown': {
                this._moveDown();
                break;
            }
            case 'ArrowUp': {
                this._moveUp();
                break;
            }
            default: {
                break;
            }
        }
    }

    _getPlayerType() {
        return 'PLAYER';
    }

    _moveLeft() {
        this.props.dispatch(movePlayerBlockThunk('left', this._getPlayerType()));
    }

    _moveRight() {
        this.props.dispatch(movePlayerBlockThunk('right', this._getPlayerType()));
    }

    _moveUp() {
        this.props.dispatch(movePlayerBlockThunk('up', this._getPlayerType()));
    }

    _moveDown() {
        this.props.dispatch(movePlayerBlockThunk('down', this._getPlayerType()));
    }
}

const mapStateToProps = (state) => ({
    difficulty: state.puzzleDifficulty,
    alertMessage: state.alertMessage
});

export default withStyles(styles, { withTheme: true })(connect(mapStateToProps)(App));
