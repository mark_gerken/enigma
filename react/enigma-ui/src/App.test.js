import React from 'react';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import store from './redux/store';
import App from './App';
import { showAlertMessageThunk } from './redux/actions/showAlertMessageThunk';

it('can render App and toggle themes', () => {
    store.dispatch(showAlertMessageThunk('error', 'Uh Oh, ', '<strong>No known Solution!</strong>', true));

    const { getByTestId } = render(
        <Provider store={store}>
            <App />
        </Provider>
    );
    const boardWrapper = getByTestId('boardWrapper');
    expect(boardWrapper).toBeTruthy();

    const removeAllAlertsButton = getByTestId('removeAllAlertsButton');
    expect(removeAllAlertsButton).toBeTruthy();

    getByTestId('openMenuButton').click();

    const themeToggle = getByTestId('themeToggle');
    themeToggle.click();
    themeToggle.click();
    themeToggle.click();
});