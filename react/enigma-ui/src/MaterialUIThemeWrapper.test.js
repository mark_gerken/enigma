import React from 'react';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import store from './redux/store';
import MaterialUIThemeWrapper from './MaterialUIThemeWrapper';

it('can render MaterialUIThemeWrapper', () => {
    const { getByTestId } = render(
        <Provider store={store}>
            <MaterialUIThemeWrapper></MaterialUIThemeWrapper>
        </Provider>
    );
    const boardWrapper = getByTestId('boardWrapper');
    expect(boardWrapper).toBeTruthy();

    getByTestId('openMenuButton').click();

    const themeToggle = getByTestId('themeToggle');
    expect(themeToggle).toBeTruthy();
});