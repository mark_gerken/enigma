import React from 'react';
import App from './App';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { unstable_createMuiStrictModeTheme as createMuiTheme } from '@material-ui/core';


const muiTheme = createMuiTheme({
    palette: { type: 'dark' }
});

function MaterialUIThemeWrapper() {
    return (
        <MuiThemeProvider theme={muiTheme}>
            <App />
        </MuiThemeProvider>
    );
}

export default MaterialUIThemeWrapper;
