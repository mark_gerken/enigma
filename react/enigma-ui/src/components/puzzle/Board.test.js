import React from 'react';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import store from '../../redux/store';
import Board from './Board';
import { changeDifficulty } from '../../redux/actions/changeDifficulty';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

var mock = new MockAdapter(axios);
const puzzleResponse = { data: {} };
mock.onGet(/\/api\/puzzle(.+)/g).reply(200, puzzleResponse);

it('can render Board with easy difficulty as default', () => {
    store.dispatch(changeDifficulty('EASY'));
    const { getByTestId } = render(
        <Provider store={store}>
            <Board />
        </Provider>
    );
    const board = getByTestId('board');
    expect(board).toBeTruthy();
});

it('can render Board with medium difficulty as default', () => {
    store.dispatch(changeDifficulty('MEDIUM'));
    const { getByTestId } = render(
        <Provider store={store}>
            <Board />
        </Provider>
    );
    const board = getByTestId('board');
    expect(board).toBeTruthy();
});

it('can render Board with hard difficulty as default', () => {
    store.dispatch(changeDifficulty('HARD'));
    const { getByTestId } = render(
        <Provider store={store}>
            <Board />
        </Provider>
    );
    const board = getByTestId('board');
    expect(board).toBeTruthy();
});

