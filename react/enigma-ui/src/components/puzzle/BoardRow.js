import React from 'react';
import { useSelector, shallowEqual } from 'react-redux';
import './BoardRow.css';
import Square from './Square';

function BoardRow(props) {
    const blockTypes = useSelector(
        (state) => {
            const rowData = (state.gameBoard.puzzle.map.blocks[props.rowNumber] || {});
            const realRowData = {};
            for (let i = 0; i < props.rowWidth; i++) {
                realRowData[i] = rowData[i] ? rowData[i].type : "EMPTY";
            }
            return realRowData;
        },
        shallowEqual
    );

    const portalColors = useSelector(
        (state) => {
            const rowData = (state.gameBoard.puzzle.map.blocks[props.rowNumber] || {});
            const realRowData = {};
            for (let i = 0; i < props.rowWidth; i++) {
                realRowData[i] = rowData[i] && rowData[i].red ? `${rowData[i].red}, ${rowData[i].green}, ${rowData[i].blue}` : null;
            }
            return realRowData;
        },
        shallowEqual
    );

    return (
        <div className="BoardRow">
            {
                Object.keys(blockTypes).sort().map((x, i) => <Square x={i} y={props.rowNumber} key={`${i}_${props.rowNumber}_s`} blockType={blockTypes[i]} portalColor={portalColors[i]}></Square> )
            }
        </div>
    );
}

export default BoardRow;
