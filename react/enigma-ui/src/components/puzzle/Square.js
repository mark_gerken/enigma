import React from 'react';
import './Square.css';

function Square(props) {
    return (
        <div className={`square ${props.blockType}`} key={`${props.x}_${props.y}_d`} style={props.portalColor ? {"borderColor": `rgb(${props.portalColor})`} : {}}></div>
    );
}

export default Square;
