import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import './Board.css';
import BoardRow from './BoardRow';
import { loadNewGameThunk } from '../../redux/actions/loadNewGameThunk';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCircleNotch } from '@fortawesome/free-solid-svg-icons';
import { showAlertMessageThunk } from '../../redux/actions/showAlertMessageThunk';

function Board(props) {
    const dispatch = useDispatch();
    const width = useSelector((state) => state.gameBoard?.puzzle?.map?.width);
    const height = useSelector((state) => state.gameBoard?.puzzle?.map?.height);
    const isLoading = useSelector((state) => state.loadingPuzzleReducer);
    const difficulty = useSelector((state) => state.puzzleDifficulty);
    let firstLoad = false;

    useEffect(() => {
        firstLoad = true;
        const goalDescription =
            "<div style='display: flex; flex-direction: row; justify-content: center;'><div class='square PLAYER' style='margin-right: 5px; height: 40px; width: 40px;'></div> <span style='margin-top: 10px;'>--></span> <div style='margin-left: 5px; height: 40px; width: 40px;' class='square END'></div></div><center><div><strong>(swipe or use arrow keys)</strong></div></center>";
        dispatch(showAlertMessageThunk('info', 'Move the player block to the goal.', goalDescription));

        if (difficulty === 'EASY') {
            dispatch(showAlertMessageThunk('success', 'Difficulty -- ', '<strong>EASY</strong>'));
        } else if (difficulty === 'MEDIUM') {
            dispatch(showAlertMessageThunk('warning', 'Difficulty -- ', '<strong>MEDIUM</strong>'));
        } else {
            dispatch(showAlertMessageThunk('error', 'Difficulty -- ', '<strong>HARD</strong>'));
        }
    }, [dispatch]);

    useEffect(() => {
        dispatch(loadNewGameThunk(difficulty, firstLoad));
    }, [dispatch, difficulty]);

    return (
        <div className="Board" data-testid="board">
            <div className={isLoading ? 'splash-screen' : 'hidden'}>
                <div className="loading-dot-container">
                    <FontAwesomeIcon className="loading-dot" icon={faCircleNotch} />
                </div>
            </div>
            {Array(height || 0)
                .fill()
                .map((a, i) => (
                    <BoardRow key={i} rowWidth={width} rowNumber={height - 1 - i}></BoardRow>
                ))}
        </div>
    );
}

export default Board;
