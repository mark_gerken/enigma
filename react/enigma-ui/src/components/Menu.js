import React, { Component } from 'react';
import './Menu.css';
import { requestSolutionThunk } from '../redux/actions/requestSolutionThunk';
import { changeDifficulty } from '../redux/actions/changeDifficulty';
import { loadNewGameThunk } from '../redux/actions/loadNewGameThunk';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Fab from '@material-ui/core/Fab';
import IconButton from '@material-ui/core/IconButton';
import AddBoxIcon from '@material-ui/icons/AddBox';
import WbIncandescentIcon from '@material-ui/icons/WbIncandescent';
import Filter1Icon from '@material-ui/icons/Filter1';
import Filter2Icon from '@material-ui/icons/Filter2';
import Filter3Icon from '@material-ui/icons/Filter3';
import Brightness4Icon from '@material-ui/icons/Brightness4';
import MenuIcon from '@material-ui/icons/Menu';
import { withStyles } from '@material-ui/core/styles';
import NightsStayIcon from '@material-ui/icons/NightsStay';

import Drawer from '@material-ui/core/Drawer';
import { showAlertMessageThunk } from '../redux/actions/showAlertMessageThunk';

const styles = (theme) => ({
    appBar: {
        top: 'auto',
        bottom: 0,
        backgroundColor: 'var(--menu-bar-background-color)'
    },
    grow: {
        flexGrow: 1
    },
    fabButton: {
        position: 'absolute',
        zIndex: 2,
        top: -10,
        left: 0,
        right: 0,
        margin: '0 auto',
        backgroundColor: 'var(--board-square-player-background) !important',
        color: 'var(--menu-bar-item-selected)'
    },
    toolBar: {
        color: 'var(--menu-bar-font-color)'
    },
    drawer: {},
    drawerItem: {
        '& > svg': {
            float: 'right'
        },
        backgroundColor: 'var(--menu-bar-background-color)',
        color: 'var(--menu-bar-item-selected)',
        padding: '0.5em'
    }
});

class Menu extends Component {
    state = {
        menuIsOpen: false,
        theme: 1
    };

    constructor(props) {
        super();
    }

    componentDidMount() {}

    render() {
        const { classes } = this.props;
        return (
            <div className={'MenuContainer ' + classes.root}>
                {/* todo; refactor this component to show the menu as-outlined on white-board, and expand//collapse appropriately */}
                <AppBar position="fixed" color="primary" className={classes.appBar} data-testid="appBar">
                    <Toolbar className={classes.toolBar}>
                        <IconButton
                            data-testid="easyButton"
                            style={this.props.difficulty === 'EASY' ? { color: 'var(--menu-bar-item-selected)' } : { color: 'inherit' }}
                            onClick={this._setDifficultyEasy.bind(this)}
                        >
                            <Filter1Icon />
                        </IconButton>
                        <IconButton
                            data-testid="mediumButton"
                            style={this.props.difficulty === 'MEDIUM' ? { color: 'var(--menu-bar-item-selected)' } : { color: 'inherit' }}
                            onClick={this._setDifficultyMedium.bind(this)}
                        >
                            <div className="firstLoadHint low">DIFFICULTY</div>
                            <Filter2Icon />
                        </IconButton>
                        <IconButton
                            data-testid="hardButton"
                            style={this.props.difficulty === 'HARD' ? { color: 'var(--menu-bar-item-selected)' } : { color: 'inherit' }}
                            onClick={this._setDifficultyHard.bind(this)}
                        >
                            <Filter3Icon />
                        </IconButton>
                        {/* <div className={classes.grow} /> */}
                        <Fab data-testid="requestSolutionButton" color="secondary" aria-label="add" className={classes.fabButton} onClick={this._requestSolution.bind(this)}>
                            <div className="firstLoadHint">SOLUTION</div>
                            <WbIncandescentIcon />
                        </Fab>
                        <div className={classes.grow} />
                        <IconButton data-testid="openMenuButton" color="inherit" onClick={this._toggleDrawer.bind(this)}>
                            <div className="firstLoadHint low">Menu</div>
                            <MenuIcon />
                        </IconButton>
                        <Drawer className={classes.drawer} anchor={'bottom'} open={this.state.menuIsOpen} onClose={this._toggleDrawer.bind(this)}>
                            <ListItem data-testid="closeMenuButton" className={classes.drawerItem} button onClick={this._toggleDrawer.bind(this)}>
                                <ListItemText primary=""></ListItemText>
                                <ListItemIcon>
                                    <MenuIcon />
                                </ListItemIcon>
                            </ListItem>
                            <ListItem
                                data-testid="newGameButton"
                                className={classes.drawerItem}
                                button
                                onClick={() => {
                                    this._onNewGameRequested();
                                    this._toggleDrawer();
                                }}
                            >
                                <ListItemText primary="New Game"></ListItemText>
                                <ListItemIcon>
                                    <AddBoxIcon />
                                </ListItemIcon>
                            </ListItem>
                            <ListItem data-testid="themeToggle" className={'themeToggle ' + classes.drawerItem} button onClick={this._toggleTheme.bind(this)}>
                                <ListItemText primary="Toggle Theme"></ListItemText>
                                <div className="switch-field">
                                    <div className={this.state.theme === 1 ? 'selected' : ''}>
                                        <NightsStayIcon />
                                    </div>
                                    <div className={this.state.theme === 0 ? 'selected' : ''}>
                                        <Brightness4Icon />
                                    </div>
                                </div>
                            </ListItem>
                        </Drawer>
                    </Toolbar>
                </AppBar>
            </div>
        );
    }

    _toggleDrawer() {
        this.setState({ menuIsOpen: !this.state.menuIsOpen });
    }

    _onNewGameRequested() {
        this.props.dispatch(loadNewGameThunk(this.props.difficulty));
    }

    _setDifficultyEasy() {
        this.props.dispatch(showAlertMessageThunk('success', 'Difficulty -- ', '<strong>EASY</strong>'));
        this._changeDifficulty('EASY');
    }

    _setDifficultyMedium() {
        this.props.dispatch(showAlertMessageThunk('warning', 'Difficulty -- ', '<strong>MEDIUM</strong>'));
        this._changeDifficulty('MEDIUM');
    }

    _setDifficultyHard() {
        this.props.dispatch(showAlertMessageThunk('error', 'Difficulty -- ', '<strong>HARD</strong>'));
        this._changeDifficulty('HARD');
    }

    _changeDifficulty(newDifficulty) {
        this.props.dispatch(changeDifficulty(newDifficulty));
    }

    _requestSolution() {
        this.props.dispatch(requestSolutionThunk());
    }

    _toggleTheme() {
        if (this.state.theme === 1) {
            this.setState({ theme: 0 });
        } else {
            this.setState({ theme: 1 });
        }
        this.props.toggleThemeCallback();
    }
}

export default withStyles(styles, { withTheme: true })(Menu);
