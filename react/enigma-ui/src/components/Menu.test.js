import React from 'react';
import { render } from '@testing-library/react';
import { changeDifficulty } from '../redux/actions/changeDifficulty';
import Menu from './Menu';

jest.mock('../redux/actions/loadNewGameThunk', () => ({
    get loadNewGameThunk() {
        return () => ({ type: 'LOAD_NEW_GAME_THUNK_MOCK' });
    }
}));

jest.mock('../redux/actions/requestSolutionThunk', () => ({
    get requestSolutionThunk() {
        return () => ({ type: 'REQUEST_SOLUTION_THUNK_MOCK' });
    }
}));
const fakeStore = (() => {
    let lastAction = null;
    let theme = 0;
    return {
        getLastAction: () => lastAction,
        dispatch: (action) => {
            lastAction = action;
        },
        getTheme: () => theme,
        toggleThemeCallback: () => {
            theme = theme === 0 ? 1 : 0;
        }
    };
})();

it('can render Menu', () => {
    const { getByTestId } = render(<Menu />);
    const appBar = getByTestId('appBar');
    expect(appBar).toBeTruthy();
});

it('can set difficulty to easy', () => {
    const { getByTestId } = render(<Menu dispatch={fakeStore.dispatch} />);
    const easyButton = getByTestId('easyButton');
    easyButton.click();
    expect(fakeStore.getLastAction()).toStrictEqual(changeDifficulty('EASY'));
});

it('can set difficulty to medium', () => {
    const { getByTestId } = render(<Menu dispatch={fakeStore.dispatch} />);
    const mediumButton = getByTestId('mediumButton');
    mediumButton.click();
    expect(fakeStore.getLastAction()).toStrictEqual(changeDifficulty('MEDIUM'));
});

it('can set difficulty to hard', () => {
    const { getByTestId } = render(<Menu dispatch={fakeStore.dispatch} />);
    const hardButton = getByTestId('hardButton');
    hardButton.click();
    expect(fakeStore.getLastAction()).toStrictEqual(changeDifficulty('HARD'));
});

it('can open the menu', () => {
    const { getByTestId } = render(
        <Menu dispatch={fakeStore.dispatch} />
    );
    const openMenuButton = getByTestId('openMenuButton');
    openMenuButton.click();
    expect(getByTestId('themeToggle')).toBeTruthy();
});

it('can close the menu', () => {
    const { getByTestId } = render(
        <Menu dispatch={fakeStore.dispatch} />
    );
    const openMenuButton = getByTestId('openMenuButton');
    openMenuButton.click();
    expect(getByTestId('closeMenuButton')).toBeTruthy();
    const closeMenuButton = getByTestId('closeMenuButton');
    closeMenuButton.click();
});

it('can request a solution', () => {
    const { getByTestId } = render(
        <Menu dispatch={fakeStore.dispatch} />
    );
    const requestSolutionButton = getByTestId('requestSolutionButton');
    requestSolutionButton.click();
    expect(fakeStore.getLastAction()).toStrictEqual({ type: 'REQUEST_SOLUTION_THUNK_MOCK' });
});

it('can toggle theme', () => {
    const { getByTestId } = render(
        <Menu dispatch={fakeStore.dispatch} toggleThemeCallback={fakeStore.toggleThemeCallback} />
    );
    getByTestId('openMenuButton').click();

    const themeToggle = getByTestId('themeToggle');
    expect(fakeStore.getTheme()).toBe(0);
    themeToggle.click();
    expect(fakeStore.getTheme()).toStrictEqual(1);
    themeToggle.click();
    expect(fakeStore.getTheme()).toStrictEqual(0);
    themeToggle.click();
    expect(fakeStore.getTheme()).toStrictEqual(1);
});

it('can request a new game', () => {
    const { getByTestId } = render(
        <Menu dispatch={fakeStore.dispatch} />
    );
    getByTestId('openMenuButton').click();

    const newGameButton = getByTestId('newGameButton');
    newGameButton.click();
    expect(fakeStore.getLastAction()).toStrictEqual({ type: 'LOAD_NEW_GAME_THUNK_MOCK' });
});
