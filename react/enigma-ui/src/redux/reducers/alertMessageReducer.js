const initialState = {};

const noKnownSolutionReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SHOW_ALERT_MESSAGE': {
            return {...state, [action.payload.id]: action.payload};
        }
        case 'REMOVE_ALERT_MESSAGE': {
            let newState = {...state};
            delete newState[action.payload.id];
            return newState;
        }
        case 'REMOVE_ALL_ALERT_MESSAGES': {
            return initialState;
        }
        default:
            return state;
    }
}

export default noKnownSolutionReducer;