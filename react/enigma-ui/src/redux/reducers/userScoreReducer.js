const initialScoreState = 0;

const userScoreStateReducer = (state = initialScoreState, action) => {
    switch (action.type) {
        case 'LOAD_PUZZLE': {
            return initialScoreState;
        }
        case 'HIT_BLOCK':
            if (action.payload.fresh) {
                return state + 1;
            } else {
                return state - 1;
            }
        default:
            return state
    }
}

export default userScoreStateReducer;