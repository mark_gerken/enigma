const initialLoadingState = false;

const loadingPuzzleReducer = (state = initialLoadingState, action) => {
    switch (action.type) {
        case 'LOAD_PUZZLE':
            return false;
        case 'LOADING_NEW_PUZZLE':
            return true;
        default:
            return state
    }
}

export default loadingPuzzleReducer;