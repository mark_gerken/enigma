const initialGameBoard = {
    puzzle: {},
    hint: {
        moves: []
    }
};

const gameBoardReducer = (state = initialGameBoard, action) => {
    switch (action.type) {
        case 'LOAD_PUZZLE': {
            const updatedRows = {};

            action.puzzle.map.playerBlocks.forEach((block) => {
                const playerX = block.x;
                const playerY = block.y;
                updatedRows[playerY] = action.puzzle.map.blocks[playerY] || {};
                updatedRows[playerY][playerX] = {
                    x: playerX,
                    y: playerY,
                    type: `${block.type} SPAWN`
                };
            });

            const updatedState = Object.assign({}, state, { hint: { moves: [] }, puzzle: action.puzzle });
            return newStateUpdatedRows(updatedState, updatedRows);
        }
        case 'CLEAR_HINTS': {
            return state.hint.moves.length ? clearHintsFromState(state) : state;
        }
        case 'SHOW_HINT': {
            return showHints(state, action);
        }
        case 'USER_MOVED': {
            const updatedRows = {};
            updatedRows[action.payload.playerY] = state.puzzle.map.blocks[action.payload.playerY] || {};
            updatedRows[action.payload.playerY][action.payload.playerX] = { type: 'EMPTY', x: action.payload.playerX, y: action.payload.playerY };
            updatedRows[action.payload.targetY] = updatedRows[action.payload.targetY] || state.puzzle.map.blocks[action.payload.targetY] || {};
            updatedRows[action.payload.targetY][action.payload.targetX] = {
                type: `${action.payload.playerType}${action.payload.playerJustSpawned ? ' SPAWN' : ''}`,
                x: action.payload.targetX,
                y: action.payload.targetY
            };

            const updatedState = {
                ...state,
                puzzle: {
                    ...state.puzzle,
                    map: {
                        ...state.puzzle.map,
                        playerBlocks: [
                            ...state.puzzle.map.playerBlocks.filter((block) => block.type !== action.payload.playerType),
                            { type: action.payload.playerType, x: action.payload.targetX, y: action.payload.targetY }
                        ]
                    }
                }
            };

            const newState = newStateUpdatedRows(updatedState, updatedRows);
            return newState;
        }
        case 'HIT_BLOCK': {
            const updatedRows = {};
            if (action.payload.fresh) {
                updatedRows[action.payload.targetY] = state.puzzle.map.blocks[action.payload.targetY] || {};
                updatedRows[action.payload.targetY][action.payload.targetX] = {
                    type: state.puzzle.map.blocks[action.payload.targetY][action.payload.targetX].type + '_HIT',
                    x: action.payload.targetX,
                    y: action.payload.targetY
                };
            } else {
                updatedRows[action.payload.targetY] = state.puzzle.map.blocks[action.payload.targetY] || {};
                updatedRows[action.payload.targetY][action.payload.targetX] = {
                    type: state.puzzle.map.blocks[action.payload.targetY][action.payload.targetX].type.split('_')[0],
                    x: action.payload.targetX,
                    y: action.payload.targetY
                };
            }

            return newStateUpdatedRows(state, updatedRows);
        }
        default: {
            return state;
        }
    }
};

function showHints(state, action) {
    const updatedRows = toggleHintsForMoveSet(state, action.payload.moves, 'EMPTY', 'EMPTY_HINT');
    return {
        ...newStateUpdatedRows(state, updatedRows),
        hint: {
            moves: [...state.hint.moves, ...action.payload.moves]
        }
    };
}

function clearHintsFromState(state) {
    const updatedRows = toggleHintsForMoveSet(state, state.hint.moves, 'EMPTY_HINT', 'EMPTY');
    return {
        ...newStateUpdatedRows(state, updatedRows),
        hint: {
            moves: []
        }
    };
}

function newStateUpdatedRows(state, updatedRows) {
    return {
        ...state,
        puzzle: {
            ...state.puzzle,
            map: {
                ...state.puzzle.map,
                blocks: Object.assign({}, state.puzzle.map.blocks, updatedRows)
            }
        }
    };
}

function toggleHintsForMoveSet(state, moves, token, replacement) {
    const toggleHintForSquare = (x, y, rows) => ({
        ...(rows[y][x] || {}),
        x,
        y,
        type: !rows[y][x] || rows[y][x].type === token ? replacement : rows[y][x].type
    });

    const indexExistsAndIsEmpty = ({ x, y }) => {
        return (
            y >= 0 &&
            x >= 0 &&
            y < state.puzzle.map.height &&
            x < state.puzzle.map.width &&
            (!state.puzzle.map.blocks[y] || !state.puzzle.map.blocks[y][x] || state.puzzle.map.blocks[y][x].type.indexOf('EMPTY') === 0)
        );
    };

    const toggleHints = (rows, coords, updateIndex) => {
        while (indexExistsAndIsEmpty(coords)) {
            rows[coords.y] = rows[coords.y] || state.puzzle.map.blocks[coords.y] || {};
            rows[coords.y][coords.x] = toggleHintForSquare(coords.x, coords.y, rows);
            coords = updateIndex(coords);
        }
    };

    const updatedRows = {};
    moves.forEach((move) => {
        if (!move.direction) {
            // no-op
        } else {
            if (move.direction.toLowerCase() === 'up') {
                toggleHints(updatedRows, { x: move.x, y: move.y + 1 }, ({ x, y }) => ({ x, y: y + 1 }));
            } else if (move.direction.toLowerCase() === 'down') {
                toggleHints(updatedRows, { x: move.x, y: move.y - 1 }, ({ x, y }) => ({ x, y: y - 1 }));
            } else if (move.direction.toLowerCase() === 'left') {
                toggleHints(updatedRows, { x: move.x - 1, y: move.y }, ({ x, y }) => ({ x: x - 1, y }));
            } else if (move.direction.toLowerCase() === 'right') {
                toggleHints(updatedRows, { x: move.x + 1, y: move.y }, ({ x, y }) => ({ x: x + 1, y }));
            }
        }
    });
    return updatedRows;
}

export default gameBoardReducer;
