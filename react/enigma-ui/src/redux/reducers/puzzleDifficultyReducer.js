const initialDifficulty = "MEDIUM";

const puzzleDifficultyReducer = (state = initialDifficulty, action) => {
    switch (action.type) {
        case 'CHANGE_DIFFICULTY': {
            return action.payload;
        }
        default:
            return state;
    }
}

export default puzzleDifficultyReducer;