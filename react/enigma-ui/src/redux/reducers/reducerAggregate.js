import { combineReducers } from 'redux'
import puzzleStateReducer from "./puzzleReducer";
import gameBoardReducer from "./gameBoardReducer";
import userScoreStateReducer from "./userScoreReducer";
import puzzleDifficultyReducer from "./puzzleDifficultyReducer";
import loadingPuzzleReducer from "./loadingPuzzleReducer";
import alertMessageReducer from "./alertMessageReducer";

export default combineReducers({
    puzzleState: puzzleStateReducer,
    gameBoard: gameBoardReducer,
    userScoreState: userScoreStateReducer,
    puzzleDifficulty: puzzleDifficultyReducer,
    alertMessage: alertMessageReducer,
    loadingPuzzleReducer
})