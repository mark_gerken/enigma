const initialPuzzleState = {
    par: 0,
    map: {}
};

const puzzleStateReducer = (state = initialPuzzleState, action) => {
    switch (action.type) {
        case 'LOAD_PUZZLE':
            return JSON.parse(JSON.stringify(action.puzzle));
        default:
            return state
    }
}

export default puzzleStateReducer;