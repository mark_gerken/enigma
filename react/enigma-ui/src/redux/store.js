import {applyMiddleware, createStore} from "redux";
import rootReducer from "./reducers/reducerAggregate.js";
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

export default createStore(rootReducer, composeWithDevTools(
    applyMiddleware(thunk)
));