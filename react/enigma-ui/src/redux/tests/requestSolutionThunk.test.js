import { requestSolutionThunk } from '../actions/requestSolutionThunk';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

const fakeStore = (() => {
    let lastAction = null;
    let state = {
        puzzleState: {
            map: {
                playerBlocks: [{}],
                blocks: {}
            }
        },
        gameBoard: {
            puzzle: {
                map: {
                    playerBlocks: [{}],
                    blocks: {}
                }
            }
        }
    };
    return {
        getLastAction: () => lastAction,
        dispatch: (action) => {
            lastAction = action;
        },
        getState: () => {
            return state;
        }
    };
})();

it('can request the next move in A solution for the active puzzle', () => {
    var mock = new MockAdapter(axios);
    const solutionResponse = { moves: [{ direction: 'left' }, { direction: 'up' }] };
    mock.onPost(/\/api\/solution/g).reply(200, solutionResponse);
    const requestOneMove = requestSolutionThunk(1);
    requestOneMove(fakeStore.dispatch, fakeStore.getState);
});

it('can request an entire solution for the active puzzle', () => {
    var mock = new MockAdapter(axios);
    const solutionResponse = { moves: [{ direction: 'left' }, { direction: 'up' }] };
    mock.onPost(/\/api\/solution/g).reply(200, solutionResponse);
    const requestWholeSolution = requestSolutionThunk();
    requestWholeSolution(fakeStore.dispatch, fakeStore.getState);
});


it('can show an error message when there is no known solution', () => {
    var mock = new MockAdapter(axios);
    mock.onPost(/\/api\/solution/g).reply(200, null);
    const requestWholeSolution = requestSolutionThunk();
    requestWholeSolution(fakeStore.dispatch, fakeStore.getState);
});
