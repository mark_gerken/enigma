import gameBoardReducer from "../reducers/gameBoardReducer";
import {movePlayerBlockThunk} from "../actions/movePlayerBlockThunk";
import { loadPuzzle } from "../actions/loadPuzzle";

const defaultPuzzle = {"par":24,"map":{"width":15,"height":27,"playerBlocks":[{"class":"com.enigma.pojo.Block","x":11,"y":13,"type":"PLAYER"}],"endBlocks":[{"class":"com.enigma.pojo.Block","x":7,"y":4,"type":"END"}],"blocks":{"0":{"1":{"class":"com.enigma.pojo.Block","x":1,"y":0,"type":"ROCK"},"3":{"class":"com.enigma.pojo.Block","x":3,"y":0,"type":"ROCK"},"6":{"class":"com.enigma.pojo.Block","x":6,"y":0,"type":"ROCK"},"8":{"class":"com.enigma.pojo.Block","x":8,"y":0,"type":"ROCK"},"10":{"class":"com.enigma.pojo.Block","x":10,"y":0,"type":"ROCK"},"14":{"class":"com.enigma.pojo.Block","x":14,"y":0,"type":"ROCK"}},"1":{"0":{"class":"com.enigma.pojo.Block","x":0,"y":1,"type":"ROCK"},"2":{"class":"com.enigma.pojo.Block","x":2,"y":1,"type":"ANGLE_2"},"9":{"class":"com.enigma.pojo.Block","x":9,"y":1,"type":"ANGLE_1"}},"2":{"0":{"class":"com.enigma.pojo.Block","x":0,"y":2,"type":"ROCK"},"3":{"class":"com.enigma.pojo.Block","x":3,"y":2,"type":"ROCK"},"4":{"class":"com.enigma.pojo.Block","x":4,"y":2,"type":"ROCK"},"13":{"class":"com.enigma.pojo.Block","x":13,"y":2,"type":"ANGLE_3"}},"3":{"7":{"class":"com.enigma.pojo.Block","x":7,"y":3,"type":"ROCK"},"12":{"class":"com.enigma.pojo.Block","x":12,"y":3,"type":"ROCK"},"13":{"class":"com.enigma.pojo.Block","x":13,"y":3,"type":"ROCK"},"14":{"class":"com.enigma.pojo.Block","x":14,"y":3,"type":"ROCK"}},"4":{"5":{"class":"com.enigma.pojo.Block","x":5,"y":4,"type":"ROCK"},"7":{"class":"com.enigma.pojo.Block","x":7,"y":4,"type":"END"},"11":{"class":"com.enigma.pojo.Block","x":11,"y":4,"type":"ROCK"},"12":{"class":"com.enigma.pojo.Block","x":12,"y":4,"type":"ANGLE_1"},"13":{"class":"com.enigma.pojo.Block","x":13,"y":4,"type":"ROCK"},"14":{"class":"com.enigma.pojo.Block","x":14,"y":4,"type":"ROCK"}},"5":{"3":{"class":"com.enigma.pojo.Block","x":3,"y":5,"type":"ROCK"},"8":{"class":"com.enigma.pojo.Block","x":8,"y":5,"type":"ROCK"}},"6":{"3":{"class":"com.enigma.pojo.Block","x":3,"y":6,"type":"ROCK"},"6":{"class":"com.enigma.pojo.Block","x":6,"y":6,"type":"ANGLE_3"}},"7":{"0":{"class":"com.enigma.pojo.Block","x":0,"y":7,"type":"ANGLE_0"},"1":{"class":"com.enigma.pojo.Block","x":1,"y":7,"type":"ROCK"},"5":{"class":"com.enigma.pojo.Block","x":5,"y":7,"type":"ANGLE_2"},"8":{"class":"com.enigma.pojo.Block","x":8,"y":7,"type":"ROCK"},"10":{"class":"com.enigma.pojo.Block","x":10,"y":7,"type":"ROCK"}},"8":{"0":{"class":"com.enigma.pojo.Block","x":0,"y":8,"type":"ROCK"},"5":{"class":"com.enigma.pojo.Block","x":5,"y":8,"type":"ANGLE_2"},"8":{"class":"com.enigma.pojo.Block","x":8,"y":8,"type":"ROCK"},"10":{"class":"com.enigma.pojo.Block","x":10,"y":8,"type":"ROCK"},"12":{"class":"com.enigma.pojo.Block","x":12,"y":8,"type":"ROCK"},"13":{"class":"com.enigma.pojo.Block","x":13,"y":8,"type":"ROCK"}},"9":{"5":{"class":"com.enigma.pojo.Block","x":5,"y":9,"type":"ANGLE_3"},"10":{"class":"com.enigma.pojo.Block","x":10,"y":9,"type":"ROCK"},"11":{"class":"com.enigma.pojo.Block","x":11,"y":9,"type":"ANGLE_2"},"14":{"class":"com.enigma.pojo.Block","x":14,"y":9,"type":"ROCK"}},"10":{"0":{"class":"com.enigma.pojo.Block","x":0,"y":10,"type":"ROCK"},"4":{"class":"com.enigma.pojo.Block","x":4,"y":10,"type":"ROCK"}},"11":{"0":{"class":"com.enigma.pojo.Block","x":0,"y":11,"type":"ROCK"},"12":{"class":"com.enigma.pojo.Block","x":12,"y":11,"type":"ROCK"},"13":{"class":"com.enigma.pojo.Block","x":13,"y":11,"type":"ROCK"},"14":{"class":"com.enigma.pojo.Block","x":14,"y":11,"type":"ROCK"}},"12":{"13":{"class":"com.enigma.pojo.Block","x":13,"y":12,"type":"ROCK"}},"13":{"1":{"class":"com.enigma.pojo.Block","x":1,"y":13,"type":"ROCK"},"3":{"class":"com.enigma.pojo.Block","x":3,"y":13,"type":"ANGLE_2"},"4":{"class":"com.enigma.pojo.Block","x":4,"y":13,"type":"ROCK"},"11":{"class":"com.enigma.pojo.Block","x":11,"y":13,"type":"PLAYER"}},"14":{"0":{"class":"com.enigma.pojo.Block","x":0,"y":14,"type":"ROCK"},"2":{"class":"com.enigma.pojo.Block","x":2,"y":14,"type":"ROCK"},"6":{"class":"com.enigma.pojo.Block","x":6,"y":14,"type":"ROCK"}},"15":{"5":{"class":"com.enigma.pojo.Block","x":5,"y":15,"type":"ROCK"},"14":{"class":"com.enigma.pojo.Block","x":14,"y":15,"type":"ROCK"}},"16":{"4":{"class":"com.enigma.pojo.Block","x":4,"y":16,"type":"ROCK"},"6":{"class":"com.enigma.pojo.Block","x":6,"y":16,"type":"ROCK"}},"17":{"2":{"class":"com.enigma.pojo.Block","x":2,"y":17,"type":"ROCK"},"3":{"class":"com.enigma.pojo.Block","x":3,"y":17,"type":"ROCK"},"7":{"class":"com.enigma.pojo.Block","x":7,"y":17,"type":"ROCK"},"9":{"class":"com.enigma.pojo.Block","x":9,"y":17,"type":"ROCK"},"14":{"class":"com.enigma.pojo.Block","x":14,"y":17,"type":"ROCK"}},"18":{"0":{"class":"com.enigma.pojo.Block","x":0,"y":18,"type":"ROCK"},"2":{"class":"com.enigma.pojo.Block","x":2,"y":18,"type":"ROCK"},"9":{"class":"com.enigma.pojo.Block","x":9,"y":18,"type":"ROCK"},"11":{"class":"com.enigma.pojo.Block","x":11,"y":18,"type":"ROCK"},"12":{"class":"com.enigma.pojo.Block","x":12,"y":18,"type":"ANGLE_2"}},"19":{"0":{"class":"com.enigma.pojo.Block","x":0,"y":19,"type":"ROCK"},"7":{"class":"com.enigma.pojo.Block","x":7,"y":19,"type":"ROCK"},"8":{"class":"com.enigma.pojo.Block","x":8,"y":19,"type":"ROCK"},"10":{"class":"com.enigma.pojo.Block","x":10,"y":19,"type":"ANGLE_1"}},"20":{"0":{"class":"com.enigma.pojo.Block","x":0,"y":20,"type":"ROCK"},"11":{"class":"com.enigma.pojo.Block","x":11,"y":20,"type":"ROCK"},"13":{"class":"com.enigma.pojo.Block","x":13,"y":20,"type":"ROCK"}},"21":{"7":{"class":"com.enigma.pojo.Block","x":7,"y":21,"type":"ROCK"},"11":{"class":"com.enigma.pojo.PortalBlock","x":11,"y":21,"type":"PORTAL","portalExitX":4,"portalExitY":23,"portalId":"11:21:4","red":187,"green":198,"blue":68},"12":{"class":"com.enigma.pojo.Block","x":12,"y":21,"type":"ROCK"},"13":{"class":"com.enigma.pojo.Block","x":13,"y":21,"type":"ROCK"}},"22":{"10":{"class":"com.enigma.pojo.Block","x":10,"y":22,"type":"ROCK"}},"23":{"4":{"class":"com.enigma.pojo.PortalBlock","x":4,"y":23,"type":"PORTAL","portalExitX":11,"portalExitY":21,"portalId":"11:21:4","red":187,"green":198,"blue":68},"9":{"class":"com.enigma.pojo.Block","x":9,"y":23,"type":"ANGLE_0"},"14":{"class":"com.enigma.pojo.Block","x":14,"y":23,"type":"ROCK"}},"24":{"4":{"class":"com.enigma.pojo.Block","x":4,"y":24,"type":"ROCK"},"6":{"class":"com.enigma.pojo.Block","x":6,"y":24,"type":"ROCK"},"10":{"class":"com.enigma.pojo.Block","x":10,"y":24,"type":"ROCK"},"13":{"class":"com.enigma.pojo.Block","x":13,"y":24,"type":"ROCK"}},"25":{"1":{"class":"com.enigma.pojo.Block","x":1,"y":25,"type":"ROCK"},"2":{"class":"com.enigma.pojo.Block","x":2,"y":25,"type":"ROCK"},"6":{"class":"com.enigma.pojo.Block","x":6,"y":25,"type":"ROCK"},"8":{"class":"com.enigma.pojo.Block","x":8,"y":25,"type":"ROCK"}},"26":{"0":{"class":"com.enigma.pojo.Block","x":0,"y":26,"type":"ROCK"},"8":{"class":"com.enigma.pojo.Block","x":8,"y":26,"type":"ROCK"},"10":{"class":"com.enigma.pojo.Block","x":10,"y":26,"type":"ROCK"},"14":{"class":"com.enigma.pojo.Block","x":14,"y":26,"type":"ROCK"}}},"numberOfIsolatedBlocks":44,"blockCount":97},"generationTime":80};
const solutionForDefaultPuzzle = {"moves":[{"x":11,"y":13,"direction":"UP","x1":11,"y1":17},{"x":11,"y":17,"direction":"UP","x1":11,"y1":17},{"x":11,"y":17,"direction":"LEFT","x1":10,"y1":17},{"x":10,"y":17,"direction":"UP","x1":10,"y1":19},{"x":10,"y":19,"direction":"LEFT","x1":9,"y1":19},{"x":9,"y":19,"direction":"UP","x1":9,"y1":23},{"x":9,"y":23,"direction":"RIGHT","x1":13,"y1":23},{"x":13,"y":23,"direction":"DOWN","x1":13,"y1":22},{"x":13,"y":22,"direction":"LEFT","x1":11,"y1":22},{"x":11,"y":22,"direction":"DOWN","x1":4,"y1":23},{"x":4,"y":23,"direction":"DOWN","x1":4,"y1":17},{"x":4,"y":17,"direction":"RIGHT","x1":6,"y1":17},{"x":6,"y":17,"direction":"UP","x1":6,"y1":23},{"x":6,"y":23,"direction":"RIGHT","x1":8,"y1":23},{"x":8,"y":23,"direction":"DOWN","x1":8,"y1":20},{"x":8,"y":20,"direction":"LEFT","x1":1,"y1":20},{"x":1,"y":20,"direction":"UP","x1":1,"y1":24},{"x":1,"y":24,"direction":"RIGHT","x1":3,"y1":24},{"x":3,"y":24,"direction":"DOWN","x1":3,"y1":18},{"x":3,"y":18,"direction":"RIGHT","x1":8,"y1":18},{"x":8,"y":18,"direction":"DOWN","x1":8,"y1":9},{"x":8,"y":9,"direction":"RIGHT","x1":9,"y1":9},{"x":9,"y":9,"direction":"UP","x1":9,"y1":16},{"x":9,"y":16,"direction":"LEFT","x1":7,"y1":16},{"x":7,"y":16,"direction":"DOWN","x1":7,"y1":4},{"x":7,"y":4,"direction":null,"x1":7,"y1":4}],"generationTime":0};

let fakeStore;

beforeEach(() => {
  fakeStore = (() => {
    let state = {gameBoard: undefined};
    let setFinalPlayerPosition = undefined;
    let setUserHasCompletedPuzzle = undefined;
    let setUserHasDied = undefined;
    let getFinalPlayerPosition = new Promise((resolve, reject) => {
      setFinalPlayerPosition = resolve; 
    });
    let getUserCompletedPuzzle = new Promise((resolve, reject) => {
      setUserHasCompletedPuzzle = resolve; 
    });
    let getUserDied = new Promise((resolve, reject) => {
      setUserHasDied = resolve; 
    });
    let playerLocation = undefined;
    let getStateImpl = () => {
      return state;
    };
    let dispatchImpl = (action) => {
      if (typeof action === "function") {
        action(dispatchImpl, getStateImpl);
      } else if (action.type === "LOAD_PUZZLE" && !action.puzzle) {
        setUserHasDied(true);
      } else {
        state.gameBoard = gameBoardReducer(state.gameBoard, action);
        if (action.type === "USER_MOVED") {
          playerLocation = {x: action.payload.targetX, y: action.payload.targetY};
        } else if (action.type === "USER_FINISHED_MOVING") {
          setFinalPlayerPosition(playerLocation);
        } else if (action.type === "USER_WON") {
          setUserHasCompletedPuzzle(true);
        }
      }
    };
    return {
        dispatch: dispatchImpl,
        getState: getStateImpl,
        getFinalPlayerPosition,
        getUserCompletedPuzzle,
        getUserDied
    };
  })();
  fakeStore.dispatch(loadPuzzle(defaultPuzzle));
});

test('player can solve puzzle', async () => {
  const initialState = fakeStore.getState();
  expect(initialState).toStrictEqual({
    gameBoard: {
      hint: {
          moves: []
      },
      puzzle: defaultPuzzle
    }
  });

  const dispatchMovesWithDelay = (moves, index, delay) => {
    if (index < moves.length) {
      setTimeout(() => {
        if (moves[index].direction) {
          fakeStore.dispatch(movePlayerBlockThunk(moves[index].direction.toLowerCase(), "PLAYER"));
        }
        dispatchMovesWithDelay(moves, index + 1, delay);
      }, delay);
    }
  };
  dispatchMovesWithDelay(solutionForDefaultPuzzle.moves, 0, 5);
  
  await expect(fakeStore.getUserCompletedPuzzle).resolves.toBe(true);
}, 30000);


test('player can move left', async () => {
  const initialState = fakeStore.getState();
  expect(initialState).toStrictEqual({
    gameBoard: {
      hint: {
          moves: []
      },
      puzzle: defaultPuzzle
    }
  });

  movePlayerBlockThunk("left", "PLAYER")(fakeStore.dispatch, fakeStore.getState);
  
  const playerBlock = defaultPuzzle.map.playerBlocks[0];

  await expect(fakeStore.getFinalPlayerPosition).resolves.not.toStrictEqual({
    x: playerBlock.x,
    y: playerBlock.y,
  });

  await expect(fakeStore.getFinalPlayerPosition).resolves.toStrictEqual({
    x: playerBlock.x-6,
    y: playerBlock.y
  });
});

test('player can move up', async () => {
  const initialState = fakeStore.getState();
  expect(initialState).toStrictEqual({
    gameBoard: {
      hint: {
          moves: []
      },
      puzzle: defaultPuzzle
    }
  });

  movePlayerBlockThunk("up", "PLAYER")(fakeStore.dispatch, fakeStore.getState);
  
  const playerBlock = defaultPuzzle.map.playerBlocks[0];

  await expect(fakeStore.getFinalPlayerPosition).resolves.not.toStrictEqual({
    x: playerBlock.x,
    y: playerBlock.y,
  });

  await expect(fakeStore.getFinalPlayerPosition).resolves.toStrictEqual({
    x: playerBlock.x,
    y: playerBlock.y + 4
  });
});

test('player can move down', async () => {
  const initialState = fakeStore.getState();
  expect(initialState).toStrictEqual({
    gameBoard: {
      hint: {
          moves: []
      },
      puzzle: defaultPuzzle
    }
  });

  movePlayerBlockThunk("down", "PLAYER")(fakeStore.dispatch, fakeStore.getState);
  
  const playerBlock = defaultPuzzle.map.playerBlocks[0];

  await expect(fakeStore.getFinalPlayerPosition).resolves.not.toStrictEqual({
    x: playerBlock.x,
    y: playerBlock.y,
  });

  await expect(fakeStore.getFinalPlayerPosition).resolves.toStrictEqual({
    x: playerBlock.x + 2,
    y: playerBlock.y - 4
  });
});

test('player can move right', async () => {
  const initialState = fakeStore.getState();
  expect(initialState).toStrictEqual({
    gameBoard: {
      hint: {
          moves: []
      },
      puzzle: defaultPuzzle
    }
  });

  movePlayerBlockThunk("right", "PLAYER")(fakeStore.dispatch, fakeStore.getState);
  
  await expect(fakeStore.getUserDied).resolves.toBe(true);
});