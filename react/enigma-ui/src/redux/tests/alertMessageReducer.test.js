import alertMessageReducer from "../reducers/alertMessageReducer";

it('returns true when a SHOW_ALERT_MESSAGE action is dispatched', () => {
    const newState = alertMessageReducer(undefined, {type: "SHOW_ALERT_MESSAGE", payload: {severity: 'error', message: 'hello there', id: 5}});
    expect(newState).toStrictEqual({5: {id: 5, severity: 'error', message: 'hello there'}});
});


it('returns a default empty state when any other action is dispatched', () => {
    const newState = alertMessageReducer(undefined, {});
    expect(newState).toStrictEqual({});
});
