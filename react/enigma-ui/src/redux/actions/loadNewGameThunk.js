import axios from 'axios';
import { loadPuzzle } from './loadPuzzle';
import { loadingNewGame } from './loadingNewGame';
import { showAlertMessageThunk } from './showAlertMessageThunk';

export const loadNewGameThunk = (difficulty, firstLoad) => (dispatch) => {
    let board = document.getElementsByClassName('Board')[0];

    if (board) {
        let width = Math.floor(board.clientWidth / 26);
        let height = Math.floor(board.clientHeight / 26) - 2;

        dispatch(loadingNewGame());

        axios
            .get(`/api/puzzle?width=${width}&height=${height}&difficulty=${difficulty}`)
            .then((res) => {
                if (firstLoad) {
                    setTimeout(() => {
                        dispatch(loadPuzzle(res.data));
                        dispatch(showAlertMessageThunk('info', 'Minimum moves to solve: ', `<strong>${res.data.par}</strong>`));
                    }, 3500);
                } else {
                    dispatch(loadPuzzle(res.data));
                    dispatch(showAlertMessageThunk('info', 'Minimum moves to solve: ', `<strong>${res.data.par}</strong>`));
                }
            })
            .catch((error) => {
                console.log("an error occured while requesting a new puzzle: " + error);
            });
    }
};
