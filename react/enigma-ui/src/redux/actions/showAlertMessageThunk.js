export const showAlertMessageThunk = (severity, message, html, includeRestartOption = false) => (dispatch) => {
    const msgId = new Date().getTime();
    const msg = { id: msgId, severity, message, html, includeRestartOption };
    dispatch({ type: 'SHOW_ALERT_MESSAGE', payload: msg });
    setTimeout(() => {
        dispatch({ type: 'REMOVE_ALERT_MESSAGE', payload: { id: msgId } });
    }, 5500);
};
