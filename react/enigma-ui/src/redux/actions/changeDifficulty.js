export const changeDifficulty = (newDifficulty) => ({
    type: 'CHANGE_DIFFICULTY',
    payload: newDifficulty
})