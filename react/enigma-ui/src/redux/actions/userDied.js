export const userDiedThunk = () => (dispatch, getState) => {
    dispatch({
        type: 'LOAD_PUZZLE',
        puzzle: getState().puzzleState
    });
};
