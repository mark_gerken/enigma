import { userMoved } from './userMoved';
import { userWonThunk } from './userWon';
import { userDiedThunk } from './userDied';
import { hitBlock } from './hitBlock';
import { userFinishedMoving } from './userFinishedMoving';
import { clearHints } from './clearHints';

export const movePlayerBlockThunk = (() => {
    // closure-variable used as a cache of queued up moves issues by the player;
    // new player move commands are stored as key-value of (current-time-MS -> direction)
    // after finishing a move it's deleted from the cache and the earliest-issued remaining command, if any, is processed
    const movementQueue = {};
    return (direction, playerType) => (dispatch, getState) => {
        movementQueue[playerType] = movementQueue[playerType] || {};
        movementQueue[playerType][new Date().getTime()] = direction;
        if (Object.keys(movementQueue[playerType]).length === 1) {
            processNextCachedMovementCommand(movementQueue[playerType], playerType, dispatch, getState);
        }
    };
})();

function processNextCachedMovementCommand(movementQueue, playerType, dispatch, getState) {
    if (!!Object.keys(movementQueue).length) {
        const gameBoard = getState().gameBoard;

        const earliestCachedTime = Object.keys(movementQueue).sort()[0];
        const direction = movementQueue[earliestCachedTime];

        // find where the player block currently is
        const playerBlock = gameBoard.puzzle.map.playerBlocks.find((block) => {
            return block.type === playerType;
        });
        const playerX = playerBlock.x;
        const playerY = playerBlock.y;

        dispatch(clearHints());

        // todo; figure out a better function here ?
        const delay = 20000 / (gameBoard.puzzle.map.height * gameBoard.puzzle.map.width);

        movePlayerBlock(movementQueue, dispatch, getState, gameBoard, direction, playerType, playerX, playerY, 1, delay);
    }
}

function clearCachedMovementCommands(movementQueue) {
    Object.keys(movementQueue).forEach((key) => delete movementQueue[key]);
}

function movePlayerBlock(movementQueue, dispatch, getState, gameBoard, direction, playerType, playerX, playerY, moveNumber, delay) {
    setTimeout(() => {
        var targetSquare = null;
        let coords = moveBlockOnce(direction, playerX, playerY);

        if (gameBoard.puzzle.map.height <= coords.y || coords.y < 0 || gameBoard.puzzle.map.width <= coords.x || coords.x < 0) {
            clearCachedMovementCommands(movementQueue);
            dispatch(userDiedThunk(playerType));
        } else {
            targetSquare = (gameBoard.puzzle.map.blocks[coords.y] || {})[coords.x];
            if (targetSquare) {
                if (targetSquare.type.indexOf('ANGLE_') === 0) {
                    coords = handleAngledBlock(targetSquare, direction, coords.x, coords.y);
                } else if (targetSquare.type === 'PORTAL') {
                    coords = handlePortalBlock(targetSquare, direction);
                }
                targetSquare = (gameBoard.puzzle.map.blocks[coords.y] || {})[coords.x];
            }

            if (!targetSquare || targetSquare.type === 'EMPTY' || targetSquare.type === 'EMPTY_HINT') {
                dispatch(userMoved(playerType, false, playerX, playerY, coords.x, coords.y));
                movePlayerBlock(movementQueue, dispatch, getState, gameBoard, coords.direction, playerType, coords.x, coords.y, moveNumber + 1, delay);
            } else {
                stopMovingPlayerBlock(dispatch, targetSquare, moveNumber, coords);
                delete movementQueue[Object.keys(movementQueue).sort()[0]];
                processNextCachedMovementCommand(movementQueue, playerType, dispatch, getState);
            }
        }
    }, delay);
}

function stopMovingPlayerBlock(dispatch, targetSquare, moveNumber, coords) {
    dispatch(userFinishedMoving());
    if (targetSquare.type === 'ROCK' && moveNumber > 1) {
        dispatch(hitBlock(coords.x, coords.y, true));
    } else if (targetSquare.type === 'ROCK_HIT' && moveNumber > 1) {
        dispatch(hitBlock(coords.x, coords.y, false));
    } else if (targetSquare.type === 'END') {
        dispatch(hitBlock(coords.x, coords.y, true));
        dispatch(userWonThunk('PLAYER'));
    }
}

function moveBlockOnce(direction, x, y) {
    if (direction === null) {
        return { x, y };
    } else if (direction === 'left') {
        return { x: x - 1, y, direction };
    } else if (direction === 'right') {
        return { x: x + 1, y, direction };
    } else if (direction === 'down') {
        return { x, y: y - 1, direction };
    } else if (direction === 'up') {
        return { x, y: y + 1, direction };
    }
}

function handlePortalBlock(targetSquare, direction) {
    let x = targetSquare.portalExitX;
    let y = targetSquare.portalExitY;
    return moveBlockOnce(direction, x, y);
}

function determineNewDirectionAfterHittingAngledBlock(direction, angleType) {
    return angleType === 'ANGLE_0'
        ? direction === 'left'
            ? 'down'
            : direction === 'up'
            ? 'right'
            : null
        : angleType === 'ANGLE_1'
        ? direction === 'right'
            ? 'down'
            : direction === 'up'
            ? 'left'
            : null
        : angleType === 'ANGLE_2'
        ? direction === 'left'
            ? 'up'
            : direction === 'down'
            ? 'right'
            : null
        : angleType === 'ANGLE_3'
        ? direction === 'right'
            ? 'up'
            : direction === 'down'
            ? 'left'
            : null
        : null;
}

function handleAngledBlock(targetSquare, direction, x, y) {
    return moveBlockOnce(determineNewDirectionAfterHittingAngledBlock(direction, targetSquare.type), x, y);
}
