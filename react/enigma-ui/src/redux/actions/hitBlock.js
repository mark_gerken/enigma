export const hitBlock = (targetX, targetY, fresh) => ({
    type: 'HIT_BLOCK',
    payload: {
        targetX, targetY, fresh
    }
})