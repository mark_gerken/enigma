import { loadNewGameThunk } from './loadNewGameThunk';
import { showAlertMessageThunk } from './showAlertMessageThunk';

export const userWonThunk = (currentPlayerIsWinner) => (dispatch, getState) => {
    dispatch(
        showAlertMessageThunk(
            'success',
            `Score: `,
            `<strong>${getState().userScoreState - 1}</strong>`
        )
    );

    dispatch({
        type: 'USER_WON',
        currentPlayerIsWinner
    });
    dispatch(loadNewGameThunk(getState().puzzleDifficulty));
};
