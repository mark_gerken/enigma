import axios from 'axios';
import { clearHints } from './clearHints';
import { showAlertMessageThunk } from './showAlertMessageThunk';

export const requestSolutionThunk = (numberOfMoves = 0) => (dispatch, getState) => {
    dispatch(clearHints());

    const puzzle = getState().puzzleState;
    const currentBoardState = getState().gameBoard;

    // todo; clean this up
    const newBlocksMap = Object.assign({}, puzzle.map.blocks, {
        [currentBoardState.puzzle.map.playerBlocks[0].y]: Object.assign({}, puzzle.map.blocks[currentBoardState.puzzle.map.playerBlocks[0].y], {
            [currentBoardState.puzzle.map.playerBlocks[0].x]: { ...puzzle.map.playerBlocks[0], ...currentBoardState.puzzle.map.playerBlocks[0] }
        })
    });

    delete newBlocksMap[puzzle.map.playerBlocks[0].y][puzzle.map.playerBlocks[0].x];

    const newPuzzle = Object.assign({}, puzzle, {
        map: Object.assign({}, puzzle.map, { playerBlocks: [{ ...puzzle.map.playerBlocks[0], ...currentBoardState.puzzle.map.playerBlocks[0] }], blocks: newBlocksMap })
    });

    axios
        .post(`/api/solution`, newPuzzle)
        .then((res) => {
            if (res.data) {
                if (numberOfMoves) {
                    res.data.moves = res.data.moves.slice(0, numberOfMoves);
                }
                if (res.data.moves.length > 1) {
                    showSolutionPart(dispatch, res.data.moves, 1);
                }
            } else {
                dispatch(showAlertMessageThunk('error', 'Uh Oh, ', '<strong>No known Solution!</strong>', true));
            }
        })
        .catch((error) => {
            console.log('an error occured while requesting a solution to the puzzle: ' + error);
        });
};

function showSolutionPart(dispatch, moves, endingIndex) {
    dispatch({
        type: 'SHOW_HINT',
        payload: { moves: [moves[endingIndex - 1]] }
    });

    setTimeout(() => {
        if (endingIndex < moves.length - 1) {
            showSolutionPart(dispatch, moves, endingIndex + 1);
        }
    }, 50);
}
