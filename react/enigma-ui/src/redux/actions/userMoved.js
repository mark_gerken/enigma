export const userMoved = (playerType, playerJustSpawned, playerX, playerY, targetX, targetY) => ({
    type: 'USER_MOVED',
    payload: {
        playerType, playerJustSpawned,
        playerX, playerY,
        targetX, targetY
    }
})