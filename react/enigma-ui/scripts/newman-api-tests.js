var fs = require('fs'),
    newman = require('newman'),

    results = [];

newman.run({
    reporters: 'htmlextra',
    collection: 'api-tests/api_tests.postman_collection.json',
    environment: 'api-tests/enigma.postman_environment.json',
    reporter: {
        htmlextra: {
            export: '../../newman.test.output.html'
        }
    }
})
.on('request', function (err, args) {
    if (!err) {
        // here, args.response represents the entire response object
        var rawBody = args.response.stream, // this is a buffer
            body = rawBody.toString(); // stringified JSON

        results.push(JSON.parse(body)); // this is just to aggregate all responses into one object
    }
})
// a second argument is also passed to this handler, if more details are needed.
.on('done', function (err, summary) {
    var performanceResults = {
        requests: results.map((result) => {
            return {
                type: result.map ? 'generation' : 'solution',
                duration: result.generationTime,
                size: result.map ? result.par : result.moves.length - 1,
                width: result.map ? result.map.width : undefined,
                height: result.map ? result.map.height : undefined
            }
        })
    };

    // write the details to any file of your choice. The format may vary depending on your use case
    fs.writeFileSync('../../newman-raw-output.json', JSON.stringify(performanceResults, null, 4));
});