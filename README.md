# Overview

<center>
  <img src="./screen1.png">
  <img src="./screen2.png">
</center>

Enigma is a spring-boot application which serves a react web-ui as static content. The jar provides a java rest api for the react application to interact with. The react web app is a puzzle game.

The puzzles are generated using a parallelized genetic algorithm leveraging an island model. The GA leverages a puzzle solver which uses breadth first search to construct a 'solution-graph' of the puzzle. The 'solution graph' is a graph where each node is unique by a key which corresponds to the X,Y coordinates within the puzzle itself. A valid puzzel has 1 player node and 1 end//solution node; the path between the two is the solution to the puzzle.

The react UI uses redux to manage its data. The actions sometimes have effects, like contacting the res-api, and leverage thunks and redux thunk middlewear to accomplish this.

The puzzle UI is entirely constructed of div elements and css styling.

# Getting started

### Development

To launch enigma in development mode you'll need to start the UI and the server seperately. 

Enigma-UI (install dependencies and run development server):

    cd react/enigma-ui/
    npm install
    npm run start

Enigma-Server (compile and run; or load into your preferred IDE and run//debug from there):

    cd ${project_root}
    mvn clean install
    java -jar target/enigma-0.0.1-SNAPSHOT.jar --server.port 8081

### Spring-Boot jar

To run enigma as a single process, a single spring-boot jar file, you will have to compile the source. This needs to be done as a two part process:

* build the react app:

        cd react/enigma-ui/
        npm install
        npm run build

* build the production jar file:

        cd ${project_root}
        mvn clean install -Pprod

Now the produced enigma*.jar file should contain the built, enigma-ui, react application inside it's static content directory. Launching the jar is the same as before:

    java -jar target/enigma-0.0.1-SNAPSHOT.jar --server.port 8081
